import re
import sys
import requests
import logging
from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import Secrets

applications = [
    ['UAID-03939 / AppGuard', '87481'],  # 487/436/0/0
    ['UAID-E0167 / eSolutions.WebLogin', '87476'],  # 50/128/0/2
    ['UAID-02590 / eSolutions.MOP', '87484'],  # 39/49/0/2
    ['UAID-E0077 / eSolutions.GANS', '87477'],  # 24/45/0/0
    ['UAID-E0282 / eSolutions.eWAS', '87475'],  # 10/39/0/0
    ['UAID-E0066 / eSolutions.ESP', '87478'],  # 4/141/0/2
    ['UAID-03113 / eSolutions.SepaWebGui', '87483'],  # 1/23/0/0
    ['UAID-E0067 / eSolutions.EDP', '89117'],  # 2/644/0/0
    ['UAID-03116 / eSolutions.CurrencyCalculator', '88939'],  # 1/7/0/0
]

# list of pages to parse
start_urls = [
    ['AppGuard', 'https://ams.fortify.com/Applications/87481/Issues'],
    # ['MOP','https://ams.fortify.com/Applications/87484/Issues'],
    # ['GANS','https://ams.fortify.com/Applications/87477/Issues'],
    # ['SepaWebGui','https://ams.fortify.com/Applications/87483/Issues'],
    # ['WebLogin','https://ams.fortify.com/Applications/87476/Issues'],
    # ['eWas','https://ams.fortify.com/Applications/87475/Issues'],
    # ['ESP','https://ams.fortify.com/Applications/87478/Issues'],
]

issue_list = [
    ['MOP', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    ['MOP', '70000035', 'Cookie Security: Overly Broad Path (4)'],
    ['MOP', '70000038', 'Cross-Site Scripting: DOM (33)'],
    ['MOP', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (11)'],
    ['MOP', '70000032', 'Header Manipulation: Cookies (2)'],
    ['MOP', '70000017', 'Log Forging (14)'],
    ['MOP', '70000001', 'Null Dereference (8)'],
    ['MOP', '70000114', 'Often Misused: Spring Remote Service (11)'],
    ['MOP', '70000000', 'Open Redirect (5)'],
    ['MOP', '70000012', 'Password Management: Hardcoded Password (3)'],
    ['MOP', '70000018', 'Password Management: Password in Configuration File (6)'],
    ['MOP', '70000028', 'Portability Flaw: Locale Dependent Comparison (7)'],
    ['MOP', '70000003', 'Privacy Violation (8)'],
    ['MOP', '70000013', 'Race Condition: Singleton Member Field (2)'],
    ['MOP', '70000009', 'Unreleased Resource: Streams (3)'],
    ['MOP', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    ['GANS', '70000102', 'ClassLoader Manipulation: Struts 1 (1)'],
    ['GANS', '70000038', 'Cross-Site Scripting: DOM (1)'],
    ['GANS', '70000002', 'Cross-Site Scripting: Reflected (424)'],
    ['GANS', '70000116', 'File Disclosure: Struts (2)'],
    ['GANS', '70000034', 'Insecure Transport: Mail Transmission (1)'],
    ['GANS', '70000057', 'Key Management: Empty Encryption Key (3)'],
    ['GANS', '70000025', 'Key Management: Hardcoded Encryption Key (1)'],
    ['GANS', '70000017', 'Log Forging (231)'],
    ['GANS', '70000001', 'Null Dereference (60)'],
    ['GANS', '70000000', 'Open Redirect (1)'],
    ['GANS', '70000012', 'Password Management: Hardcoded Password (1)'],
    ['GANS', '70000018', 'Password Management: Password in Configuration File (10)'],
    ['GANS', '70000033', 'Path Manipulation (22)'],
    ['GANS', '70000028', 'Portability Flaw: Locale Dependent Comparison (13)'],
    ['GANS', '70000003', 'Privacy Violation (555)'],
    ['GANS', '70000013', 'Race Condition: Singleton Member Field (119)'],
    ['GANS', '70000027', 'Server-Side Request Forgery (22)'],
    ['GANS', '70000039', 'SQL Injection (10)'],
    ['GANS', '70000031', 'System Information Leak: External (205)'],
    ['GANS', '70000004', 'Unreleased Resource: Database (18)'],
    ['GANS', '70000009', 'Unreleased Resource: Streams (24)'],
    ['GANS', '70000019', 'XML External Entity Injection (1)'],
    ['SepaWebGui', '70000038', 'Cross-Site Scripting: DOM (4)'],
    ['SepaWebGui', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (2)'],
    ['SepaWebGui', '70000015', 'Insecure Transport (1)'],
    ['SepaWebGui', '70000017', 'Log Forging (19)'],
    ['SepaWebGui', '70000001', 'Null Dereference (646)'],
    ['SepaWebGui', '70000018', 'Password Management: Password in Configuration File (14)'],
    ['SepaWebGui', '70000028', 'Portability Flaw: Locale Dependent Comparison (24)'],
    ['SepaWebGui', '70000003', 'Privacy Violation (256)'],
    ['SepaWebGui', '70000031', 'System Information Leak: External (38)'],
    ['SepaWebGui', '70000004', 'Unreleased Resource: Database (30)'],
    ['SepaWebGui', '70000062', 'Unreleased Resource: Files (3)'],
    ['SepaWebGui', '70000014', 'Unreleased Resource: Sockets (1)'],
    ['SepaWebGui', '70000009', 'Unreleased Resource: Streams (65)'],
    ['SepaWebGui', '70000040', 'Weak Encryption (3)'],
    ['SepaWebGui', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    ['SepaWebGui', '70000043', 'Weak SecurityManager Check: Overridable Method (8)'],
    ['WebLogin', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    ['WebLogin', '70000002', 'Cross-Site Scripting: Reflected (15)'],
    ['WebLogin', '70000057', 'Key Management: Empty Encryption Key (3)'],
    ['WebLogin', '70000017', 'Log Forging (63)'],
    ['WebLogin', '70000001', 'Null Dereference (83)'],
    ['WebLogin', '70000000', 'Open Redirect (11)'],
    ['WebLogin', '70000012', 'Password Management: Hardcoded Password (12)'],
    ['WebLogin', '70000033', 'Path Manipulation (1)'],
    ['WebLogin', '70000028', 'Portability Flaw: Locale Dependent Comparison (14)'],
    ['WebLogin', '70000003', 'Privacy Violation (54)'],
    ['WebLogin', '70000004', 'Unreleased Resource: Database (36)'],
    ['WebLogin', '70000009', 'Unreleased Resource: Streams (12)'],
    ['eWas', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (28)'],
    ['eWas', '70000015', 'Insecure Transport (2)'],
    ['eWas', '70000034', 'Insecure Transport: Mail Transmission (2)'],
    ['eWas', '70000021', 'JSON Injection (2)'],
    ['eWas', '70000017', 'Log Forging (11)'],
    ['eWas', '70000036', 'Mass Assignment: Insecure Binder Configuration (4)'],
    ['eWas', '70000068', 'Mass Assignment: Request Parameters Bound into Persisted Objects (1)'],
    ['eWas', '70000001', 'Null Dereference (6)'],
    ['eWas', '70000018', 'Password Management: Password in Configuration File (80)'],
    ['eWas', '70000041', 'Portability Flaw: File Separator (1)'],
    ['eWas', '70000028', 'Portability Flaw: Locale Dependent Comparison (4)'],
    ['eWas', '70000003', 'Privacy Violation (21)'],
    ['eWas', '70000013', 'Race Condition: Singleton Member Field (4)'],
    ['eWas', '70000027', 'Server-Side Request Forgery (1)'],
    ['eWas', '70000086', 'Spring Boot Misconfiguration: Actuator Endpoint Security Disabled (16)'],
    ['eWas', '70000014', 'Unreleased Resource: Sockets (2)'],
    ['eWas', '70000009', 'Unreleased Resource: Streams (5)'],
    ['eWas', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    ['eWas', '70000043', 'Weak SecurityManager Check: Overridable Method (2)'],
    ['eWas', '70000019', 'XML External Entity Injection (48)'],
    ['ESP', '70000102', 'ClassLoader Manipulation: Struts 1 (1)'],
    ['ESP', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    ['ESP', '70000035', 'Cookie Security: Overly Broad Path (2)'],
    ['ESP', '70000002', 'Cross-Site Scripting: Reflected (1)'],
    ['ESP', '70000034', 'Insecure Transport: Mail Transmission (2)'],
    ['ESP', '70000017', 'Log Forging (29)'],
    ['ESP', '70000001', 'Null Dereference (72)'],
    ['ESP', '70000000', 'Open Redirect (2)'],
    ['ESP', '70000012', 'Password Management: Hardcoded Password (1)'],
    ['ESP', '70000028', 'Portability Flaw: Locale Dependent Comparison (3)'],
    ['ESP', '70000003', 'Privacy Violation (2)'],
    ['ESP', '70000031', 'System Information Leak: External (1)'],
    ['ESP', '70000004', 'Unreleased Resource: Database (3)'],
    ['ESP', '70000014', 'Unreleased Resource: Sockets (1)'],
    ['ESP', '70000009', 'Unreleased Resource: Streams (26)'],
    ['ESP', '70000019', 'XML External Entity Injection (6)'],

    ['AppGuard', '70000038', 'Cross-Site Scripting: DOM (5)'],
    ['AppGuard', '70000002', 'Cross-Site Scripting: Reflected (159)'],
    ['AppGuard', '70000022', 'Denial of Service: Regular Expression (8)'],
    ['AppGuard', '70000010', 'Dynamic Code Evaluation: Code Injection (2)'],
    ['AppGuard', '70000006', 'Header Manipulation (1)'],
    ['AppGuard', '70000001', 'Null Dereference (1)'],
    ['AppGuard', '70000000', 'Open Redirect (93)'],
    ['AppGuard', '70000018', 'Password Management: Password in Configuration File (45)'],
    ['AppGuard', '70000085', 'Password Management: Password in HTML Form (4)'],
    ['AppGuard', '70000028', 'Portability Flaw: Locale Dependent Comparison (8)'],
    ['AppGuard', '70000003', 'Privacy Violation (33)'],
    ['AppGuard', '70000044', 'Privacy Violation: Autocomplete (11)'],
    ['AppGuard', '70000013', 'Race Condition: Singleton Member Field (12)'],
    ['AppGuard', '70000027', 'Server-Side Request Forgery (6)'],
    ['AppGuard', '70000031', 'System Information Leak: External (40)'],
    ['AppGuard', '70000009', 'Unreleased Resource: Streams (7)'],
    ['AppGuard', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    ['AppGuard', '70000043', 'Weak SecurityManager Check: Overridable Method (1)'],
]

SCANNED = '27410'
ANALYSIS = '27411'
IN_REMEDIATION = '27412'
SUBMITTED_FOR_RESCAN = '27413'

RED = '27404'
AMBER = '27405'
GREEN = '27406'

# Update app status
pl = {"ApplicationId": 89117,
      "AttributeValues": [
          {"Id": 6907, "Value": "UAID-E0067"},
          {"Id": 6920, "Value": "0"},
          {"Id": 6931, "Value": "0"},
          {"Id": 6932, "Value": "0"},
          {"Id": 6934, "Value": ""},
          {"Id": 6936, "Value": ""},
          {"Id": 6939, "Value": "2018/01/05"},  # planning date
          {"Id": 6940, "Value": "2018/01/31"},  # remediation date
          {"Id": 6941, "Value": "2018/02/02"},  # code delivery
          {"Id": 6942, "Value": "27406"},  # 27404 red, 27405 amber, 27406 green
          {"Id": 6943, "Value": ""},
          # scanned 27410, analysis 27411, in remediation 27412, submitted for rescan 27413
          {"Id": 6946, "Value": "27411"},
          {"Id": 6947, "Value": "0"}
      ]
      }


def pretty_print_POST(req):
    """
    At this point it is completely built and ready
    to be fired; it is "prepared".

    However pay attention at the formatting used in
    this function because it is programmed to be pretty
    printed and may differ from the actual request.
    """
    print('{}\n{}\n{}\n\n{}'.format(
        '-----------START-----------',
        req.method + ' ' + req.url,
        '\n'.join('{}: {}'.format(k, v) for k, v in req.headers.items()),
        req.body,
    ))


def print_applications():
    regex = re.compile('/Applications/(\d+)..title=.(UAID-[E0]\d+ / [^\"]+)|<div>(\d+)</div>', re.MULTILINE)
    result = s.get("https://ams.fortify.com/Applications").text
    matches = iter(regex.finditer(result))
    for m in matches:
        critical = next(matches).group(3)
        high = next(matches).group(3)
        medium = next(matches).group(3)
        low = next(matches).group(3)
        print "{:<50}{:<10}{}{:>4}{}{:>4}{}{:>4}{}{:>4}".format("['" + m.group(2) + "',", "'" + m.group(1) + "'],",
                                                                " # ", critical, "/", high, "/", medium, "/", low)


def print_application_scans():
    regex = re.compile('<td>(20\d\d/\d\d/\d\d)</td>|severity-badge [^>]+>(\d+)', re.MULTILINE)

    for app in applications:

        sys.stdout.write("\n")
        sys.stdout.write(app[0])
        result = s.get("https://ams.fortify.com/Applications/" + app[1] + "/Scans").text
        matches = iter(regex.finditer(result))
        for m in matches:
            date = m.group(1)
            issues = m.group(2)
            if date is not None:
                completed = next(matches).group(1)
                if completed is None:
                    completed = '--------'
                sys.stdout.write("\n    Started: " + date + "  Completed: " + completed + "     ")
            elif issues is not None:
                sys.stdout.write("{:>6}".format(issues))
                # sys.stdout.write(" / ")

            sys.stdout.flush()


def get_issue_ids(query_string):
    ids = set()
    result = s.get(query_string.replace('/Issues?', '/IssuesData?')).json()
    # print result
    for i in result['Items']:
        _id = i['UniqueId']
        # print _id
        ids.add(_id)
    return ids


def get_application_id(query_string):
    # '<input id="hiddenApplicationId" name="hiddenApplicationId" type="hidden" value="87481" />'

    # <input name="__RequestVerificationToken" type="hidden" value="kaS2z4F6QRdslO-EqGCX0O92VTUvXrv-a8ihwBVNd67hdNkik-70Xy-0m5oZQLOz1gFEIkDpVqbrQIP9XWK04UdoLHS8wmvoQmA2WH-L3tFqnKZgN-SfjleJaTOm28_92DR2bA2" />
    result = s.get(query_string).text

    regex = re.compile('hiddenApplicationId.*value=.(\d+)[^\d]', re.MULTILINE)
    matches = regex.findall(result)
    app_id = str(matches[0])

    regex = re.compile('__RequestVerificationToken.*value="([^"]+)', re.MULTILINE)
    matches = regex.findall(result)
    token = str(matches[0])

    return app_id, token


def mark_false_positive(query_string, justification):
    issue_ids = get_issue_ids(query_string)
    app_id, token = get_application_id(query_string)

    print "Processing", len(issue_ids), "issues."

    headers = {'__RequestVerificationToken': token,
               'Content-Type': 'application/json; charset=UTF-8',
               'Referer': query_string,
               'X-Requested-With': 'XMLHttpRequest',
               'Origin': 'https://ams.fortify.com',
               }

    payload = {
        "ProjectId": str(app_id),
        "Content": "[FPC] 1.) Please describe why you believe this issue is a false positive.: " + justification
    }

    sys.stdout.write("Step 1 ")
    sys.stdout.flush()

    for _id in issue_ids:
        payload['FindingUniqueId'] = str(_id)
        r = s.post("https://ams.fortify.com/Findings/AddComment", json=payload, headers=headers)
        sys.stdout.write(".")
        sys.stdout.flush()
        if r.status_code != 200:
            print "\nERROR POST 1:", r.status_code

    sys.stdout.write("\nStep 2 ")
    sys.stdout.flush()

    payload["Content"] = "[FPC] 2.) Please specify the file name and line number in the submitted source code that supports your false positive challenge.: N/A"
    for _id in issue_ids:
        # print _id
        payload['FindingUniqueId'] = str(_id)
        r = s.post("https://ams.fortify.com/Findings/AddComment", json=payload, headers=headers)
        sys.stdout.write(".")
        sys.stdout.flush()
        if r.status_code != 200:
            print "\nERROR POST 2:", r.status_code

    sys.stdout.write("\nStep 3 ")
    sys.stdout.flush()

    payload["Content"] = "[FPC] Additional Comments: N/A"
    for _id in issue_ids:
        # print _id
        payload['FindingUniqueId'] = str(_id)
        r = s.post("https://ams.fortify.com/Findings/AddComment", json=payload, headers=headers)
        sys.stdout.write(".")
        sys.stdout.flush()
        if r.status_code != 200:
            print "\nERROR POST 3:", r.status_code

    sys.stdout.write("\nStep 4 ")
    sys.stdout.flush()

    payload = {
        "ProjectId": str(app_id),
        "Challenge": 1
    }
    for _id in issue_ids:
        # print _id
        payload['FindingUniqueId'] = str(_id)
        r = s.post("https://ams.fortify.com/Findings/SetFalsePositiveChallenge", json=payload, headers=headers)
        sys.stdout.write(".")
        sys.stdout.flush()
        if r.status_code != 200:
            print "ERROR POST 4:", r.status_code

    print "\nMarked", len(issue_ids), "issues as false positive."


def print_issues_by_category():
    # regex = re.compile("<a href=.*")
    regex = re.compile('<a href="/Applications/\d+/Issues\?d=(7\d+)" title="([^"]*)"', re.MULTILINE)

    for start_url in start_urls:
        application = start_url[0]
        url = start_url[1]
        r = s.get(url)
        # print application, "=", r.status_code
        result = r.text
        # print result
        matches = regex.findall(result)
        for m in matches:
            print "['" + application + "', '" + m[0] + "', '" + m[1] + "'],"


urllib3.disable_warnings(InsecureRequestWarning)
s = requests.Session()
s.proxies = {"http": "http://"+Secrets.proxy_pwd, "https": "http://"+Secrets.proxy_pwd}
# s.trust_env=False

payload = {'UserName': "vladimir.hrmo@firstdata.com", 'Password': "#MojeHeslo09", 'TenantCode': "firstdata"}
r = s.post('https://ams.fortify.com/Account/TenantLogin', data=payload, verify=False)
print "Authentication:", r.status_code

print_applications()
print_application_scans()
# print_issues()

# ids = get_issue_ids("https://ams.fortify.com/Releases/138695/Issues?d=140000003+70000018&g=1000006&t=-5105")
# print ids

# print get_application_id("https://ams.fortify.com/Releases/138695/Issues?d=140000003+70000018&g=1000006&t=-5105")
#
# mark_false_positive("https://ams.fortify.com/Releases/138690/Issues?g=1000006&t=-5105&d=140000003",
#                     "False positive - please see comments from Radovan Polakovic with more info on why we think this is false positive.")

# mark_false_positive("https://ams.fortify.com/Releases/138690/Issues?g=1000006&t=-5102&srch=EAMAuditLogger",
#                     "StringEscapeUtils.escapeHtml4 is used to encode the user input before logging it")

