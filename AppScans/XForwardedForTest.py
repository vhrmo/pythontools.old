import requests
import urllib3
from urllib3.exceptions import InsecureRequestWarning

import Secrets

urllib3.disable_warnings(InsecureRequestWarning)

s = requests.Session()
# s.proxies = {"http": "http://"+Secrets.proxy_pwd, "https": "http://"+Secrets.proxy_pwd}

headers = {'X-Forwarded-For': '10.2.2.3'}

# r = s.get('https://online-qa.firstdata.de/bzzz', headers=headers)
# r = s.get('https://cat-sso.1dc.com/sso/bzzz', headers=headers)
r = s.get('https://misc-u-i.firstdata.eu/bzzz')
# r = s.get('https://misc-u-i.firstdata.eu/bzzz', headers=headers)
print r.status_code



# def setup_proxy():
#     urllib3.install_opener(
#         urllib3.build_opener(
#             urllib3.ProxyHandler({'https': Secrets.proxy_pwd})
#         )
#     )
# setup_proxy()

# r = urllib2.urlopen('https://online.firstdata.de/login')
# r = urllib2.urlopen('https://insight.aibms.com')
# print r.info()

# r.geturl() # return the URL of the resource retrieved, commonly used to determine if a redirect was followed
# r.info() # return the meta-information of the page, such as headers, in the form of an mimetools. Message instance.
# r.getcode() # return the HTTP status code of the response.


