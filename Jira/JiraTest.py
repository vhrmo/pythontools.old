from jira import JIRA
import urllib3
import json
import Secrets



urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

ops = {
    'server': 'https://jiratest.1dc.com',
    # 'server': 'https://jirasdlc.1dc.com',
    'verify': False,

    "headers": {
        'Cache-Control': 'no-cache',
        # 'Accept': 'application/json;charset=UTF-8',  # default for REST
        'Content-Type': 'application/json',  # ;charset=UTF-8',
        'Accept': 'application/json',  # default for REST
        'Pragma': 'no-cache',
        # 'Expires': 'Thu, 01 Jan 1970 00:00:00 GMT'
        'X-Atlassian-Token': 'no-check'}

}

jira = JIRA(basic_auth=(Secrets.user_name, Secrets.pwd), options=ops)    # a username/password tuple


# Find all issues reported by ...
# print issues

# Get an issue.
# parent_issue = jira.issue('TASK-7384')

# Change the issue's summary and description.
# issue.update(
#     summary="I'm different!", description='Changed the summary to be different.')

# You can update the entire labels field like this
# issue.update(labels=['AAA', 'BBB'])

# Linking a remote jira issue (needs applinks to be configured to work)
# issue = jira.issue('JRA-1330')
# issue2 = jira.issue('XX-23')
# jira.add_simple_link(issue, issue2)

# issue = jira.issue("TASK-7385")
# print json.dumps(issue.raw, indent=2, sort_keys=True)
# print issue.raw

# new_issue_dict = ""
# print jira.create_issue(fields=new_issue_dict)

# new_issue = jira.create_issue(project='TASK',
#                               summary='New issue from jira-python',
#                               # description='Look into this one',
#                               issuetype={'name': 'Sub Task'},
#                               parent={'key': 'TASK-7384'})
#
# print json.dumps(new_issue, indent=2, sort_keys=True)

def print_create_meta_data():
    meta = jira.createmeta(projectKeys='TASK', expand="projects.issuetypes.fields")
    print json.dumps(meta, indent=2, sort_keys=True)


def print_issue(key):
    issue = jira.issue(key)
    print json.dumps(issue.raw, indent=2, sort_keys=True)


def create_task():
    ft = jira.issue('FT-1409')

    issue_dict_task = {
        "project": {"key": "TASK"},
        "issuetype": {"name": "Task"},
        "components": [{"name": "eSolutions.Merchant Online Portal.Germany"}],
        "customfield_13113": "jira-internal-all",  # Contributor groups
        "summary": "test issue",
        "description": "Description",
        "duedate": "2017-11-17",
        "customfield_10100": {  # assignee
            "id": "tsku1290",
        },
        "labels": [
            "FOD"
        ],
        # "customfield_12608": "FT-1409",
        # "customfield_12609": "FT-1409: eSolutions - FOD Vulnerability fixing",
        # "customfield_12664": "FT-1409",
        # "customfield_10121": 40.0,  # effort
    }
    new_issue = jira.create_issue(fields=issue_dict_task)
    print new_issue.key

    jira.create_issue_link(type=u'Parent Issue <=> Task', outwardIssue='FT-1409', inwardIssue=new_issue.key)



# Sub task creation is broken in SDLC JIRA
def create_sub_task():
    parent_issue = jira.issue('TASK-7384') # JIRA TEST
    # parent_issue = jira.issue('TASK-2211') # JIRA SDLC

    issue_dict_sub_task = {
        "project": {"key": "TASK"},
        "issuetype": {"name": "Sub Task"},
        # "components": [{"name": "eSolutions.Merchant Online Portal.Germany"}],
        # "customfield_13113": "jira-internal-all",  # Contributor groups
        # "Issue Summary": "Summaryyyyyyyy",
        # "summary": "summmmmmmmmmmmmary",
        # "customfield_12663": "summmmmmmmmmmmmary",
        # "customfield_10203": "summmmmmmmmmmmmary",
        # "customfield_12616": 12133,
        # "customfield_12009": "eSolutions.AMOS.Germany",
        # "customfield_10204": "desrrrrrrrrrrrrrrrrrr",
        # "customfield_10100": "user-id",
        # "labels": "test_label",
        # "customfield_10121": 154,
        # "customfield_10211": "30.11.2017",
        # "description": "Description",
        # "duedate": "2017-11-17",
        # "customfield_10121": 40.0,  # effort
        "parent": {
            "id": parent_issue.id,
        },
        # "assignee": {  # assignee
        #     "id": "user-id",
        # },
    }

    new_issue = jira.create_issue(fields=issue_dict_sub_task)
    print new_issue.key


print_issue('TASK-7424')
# create_task()
# print jira.issue_link_types()
# print json.dumps(link_types, indent=2, sort_keys=True)

# jira.comment

# create_sub_task()
# jira.issue_type()

