import re
import sys

import requests
import urllib3
from jira import JIRA
# from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import Secrets
import Users

ACTION_REASSIGN_SUBTASK = "161"
ACTION_REASSIGN_TASK = "111"
ACTION_CLOSE_SUBTASK = "111" # is this correct?
ACTION_CLOSE = "141"
ACTION_REOPEN = "151"
ACTION_START_PROGRESS = "181"
ACTION_REVIEW = "281"
ACTION_UPDATE_PARENT = "311"
ACTION_EDIT_ISSUE = '521'

user = Secrets.user_name
pwd = Secrets.pwd
# jira_url = 'https://jiratest.1dc.com'
jira_url = 'https://jirasdlc.1dc.com'

ops = {
    'server': jira_url,
    'verify': False,
}

urllib3.disable_warnings(InsecureRequestWarning)

s = requests.Session()
r = s.get(jira_url, auth=(user, pwd), verify=False)
print "Authentication:", r.status_code
# print r.headers

jira = JIRA(basic_auth=(user, pwd), options=ops)  # a username/password tuple

atl_token_regex = re.compile('JIRA.XSRF.updateTokenOnPage..([^"]*)"', re.MULTILINE)
guid_regex = re.compile('id=.guid.[ ]*name=.guid.[ ]*value=.([\d]+).', re.MULTILINE)
atl_token = None


def get_atl_token():
    global atl_token
    if atl_token is not None:
        return atl_token

    # payload to get the atl_token value
    payload = {
        "inline": "true",
        "decorator": "dialog",
        "action": "321",  # create sub task
        # "id": p.id,  # id of a parent task
    }

    r = s.post(jira_url + '/secure/CommentAssignIssue.jspa', data=payload, verify=False)
    print "GetAtlToken:", r.status_code
    result = r.text
    # print result

    # Get the security token from the first attempt - needs to be included in the subsequent attempts
    token = ""
    matches = atl_token_regex.findall(result)
    for m in matches:
        token = m

    atl_token = token
    print 'Token:', atl_token
    return atl_token


def extract_guid(html):
    html = html.replace('\n', ' ')
    # print html

    matches = guid_regex.findall(html)
    for m in matches:
        print 'guid: ' + m
        return m

    return None


def create_task(summary, description, assignee, due_date, component, hours=None, fast_track_key=None, label=None):
    payload = {
        "project": {"key": "TASK"},
        "issuetype": {"name": "Task"},
        "components": [{"name": component}],
        "customfield_13113": "jira-internal-all",  # Contributor groups
        'customfield_11901': [{'id': 'f2j13l5'}, {'id': 'f3ppgtk'}],  # Contributors
        "summary": summary,
        "description": description,
        "duedate": due_date,
    }

    if hours is not None:
        payload['customfield_10121'] = hours

    if label is not None:
        payload['labels'] = [label]

    new_issue = jira.create_issue(fields=payload)
    print new_issue.key

    reassign_issue(new_issue, assignee)

    if fast_track_key is not None:
        update_parent(new_issue, fast_track_key)

    return new_issue


def create_sub_task(parent, summary, description, assignee, hours, due_date, components, label=None):
    p = jira.issue(parent)

    token = get_atl_token()

    payload = {
        "inline": "true",
        "decorator": "dialog",
        "action": "321",  # create sub task
        "id": p.id,  # id of a parent task
        "atl_token": token,
        "customfield_10203": summary,
        # "customfield_12616": "13203",  ## ???
        "customfield_12009": components,
        "customfield_10204": description,
        "customfield_10100": assignee,
        "customfield_10121": hours,
        "customfield_10211": due_date,
    }

    if label is not None:
        payload['labels'] = label

    r = s.post(jira_url + '/secure/CommentAssignIssue.jspa', data=payload, verify=False)
    print "POST:", r.status_code
    # print  r.text


def convert_sub_task_to_task(issue):
    token = get_atl_token()

    page = s.get(jira_url + '/secure/ConvertSubTask.jspa?id=' + issue.id)
    guid = extract_guid(page.text)

    payload = {
        'issuetype': '10701',
        'id': issue.id,
        'guid': guid,
        # 'Next >>': 'Next >>',
        'priority': '8',
        'atl_token': token,
    }
    # print payload

    r = s.post(jira_url + '/secure/ConvertSubTaskSetIssueType.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".")
    sys.stdout.flush()

    r = s.post(jira_url + '/secure/ConvertSubTaskUpdateFields.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".")
    sys.stdout.flush()

    payload['Finish'] = 'Finish'
    r = s.post(jira_url + '/secure/ConvertSubTaskConvert.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".\n")
    sys.stdout.flush()
    # print  r.text


def convert_task_to_sub_task(issue, parent_key):
    token = get_atl_token()

    # clear parent for the issue which will become child
    update_parent(issue, '')

    page = s.get(jira_url + '/secure/ConvertIssue.jspa?id=' + issue.id)
    guid = extract_guid(page.text)

    payload = {
        'parentIssueKey': parent_key,
        'issuetype': '10702',
        'id': issue.id,
        'guid': guid,
        # 'Next >>': 'Next >>',
        'atl_token': token,
    }
    # print payload

    r = s.post(jira_url + '/secure/ConvertIssueSetIssueType.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".")
    sys.stdout.flush()

    r = s.post(jira_url + '/secure/ConvertIssueUpdateFields.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".")
    sys.stdout.flush()

    payload['Finish'] = 'Finish'
    r = s.post(jira_url + '/secure/ConvertIssueConvert.jspa', data=payload, verify=False)
    sys.stdout.write(str(r.status_code) + ".\n")
    sys.stdout.flush()
    # print  r.text


# Move subtask to another task as a subtask - two step process:
#   1. convert to task,
#   2. convert to subtask of a new parent task
def move_sub_task(issue, new_parent_key):
    convert_sub_task_to_task(issue)
    convert_task_to_sub_task(issue, new_parent_key)


def search_sub_tasks(parent_task):
    tasks = jira.search_issues("parent = " + parent_task)
    return tasks


def update_issue(issue, action, new_assignee=None, comment=None, new_parent_task=None, new_due_date=None):
    token = get_atl_token()

    payload = {
        "inline": "true",
        "decorator": "dialog",
        "action": action,
        "id": issue.id,
        # viewIssueKey =
        # commentLevel =
        "atl_token": token,
    }

    if new_assignee is not None:
        payload['customfield_10100'] = new_assignee

    if comment is not None:
        payload['comment'] = comment

    if new_parent_task is not None:
        payload['customfield_12664'] = new_parent_task

    if new_due_date is not None:
        payload['duedate'] = new_due_date

    print payload

    r = s.post(jira_url + '/secure/CommentAssignIssue.jspa', data=payload, verify=False)
    print "POST:", r.status_code
    # print  r.text


def set_fix_version(issue, new_fix_version):
    token = get_atl_token()
    # 'https://jirasdlc.1dc.com/secure/AjaxIssueAction.jspa?decorator=none'

    payload = {
        'fixVersions': new_fix_version,
        'issueId': issue.id,
        'singleFieldEdit': 'true',
        'fieldsToForcePresent': 'fixVersions',
        'atl_token': token,
    }


    r = s.post(jira_url + '/secure/AjaxIssueAction.jspa', data=payload, verify=False)
    print "POST:", r.status_code
    # print  r.text


def update_due_date(issue, new_due_date):
    update_issue(issue, ACTION_EDIT_ISSUE, new_due_date=new_due_date)


def update_due_dates(issues, new_due_date):
    for issue in issues:
        update_due_date(issue, new_due_date=new_due_date)


def update_due_dates_jql(jql, new_due_date):
    issues = jira.search_issues(jql)
    update_due_dates(issues, new_due_date=new_due_date)


def close_issue(issue, comment):
    if issue.fields.issuetype.name == 'Task':
        update_issue(issue, ACTION_CLOSE, comment=comment)
    else:
        update_issue(issue, ACTION_CLOSE_SUBTASK, comment=comment)


def reassign_issue(issue, new_assignee, comment=None):
    if issue.fields.issuetype.name == 'Task':
        update_issue(issue, ACTION_REASSIGN_TASK, new_assignee=new_assignee, comment=comment)
    else:
        update_issue(issue, ACTION_REASSIGN_SUBTASK, new_assignee=new_assignee, comment=comment)


# Updates link to FAST track - doesn't work for sub tasks
def update_parent(issue, new_parent_task, comment=None):
    update_issue(issue, ACTION_UPDATE_PARENT, new_parent_task=new_parent_task, comment=comment)


def reassign_issues_jql(jql, new_assignee, comment=""):
    issues = jira.search_issues(jql)
    reassign_issues(issues, new_assignee, comment)


def reassign_issues(issues, new_assignee, comment=""):
    for issue in issues:
        reassign_issue(issue, new_assignee, comment)


def update_issues_status_jql(jql, action, comment):
    issues = jira.search_issues(jql)
    update_issues_status(issues, action, comment)


def update_issues_status(issues, action, comment):
    for issue in issues:
        update_issue(issue, action=action, comment=comment)


# Updates link to fast track
def update_issues_parent_jql(jql, new_parent_task):
    issues = jira.search_issues(jql)
    update_issues_parent(issues, new_parent_task)


# Updates link to fast track
def update_issues_parent(issues, new_parent_task):
    for issue in issues:
        update_parent(issue, new_parent_task)


def create_automation_sub_tasks(prefix, parent_task, due_date, component):
    label = 'TLS'
    create_sub_task(parent=parent_task, summary=prefix + 'Jenkins verify-build job for all applications',
                    description='Verify-build jobs. Result should be package in nexus.', assignee='favr97v', hours='20',
                    due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task, summary=prefix + 'Rundeck deploy appserver',
                    description='Be able to deploy only config, create instances, or get instances from GIT',
                    assignee='favr97v', hours='20', due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task, summary=prefix + 'Rundeck deploy applications',
                    description='Take application from nexus and deploy it to server', assignee='favr97v', hours='20',
                    due_date=due_date, components=component, label=label)


def create_server_setup_sub_tasks(parent_task_key, prefix, due_date, component):
    print parent_task_key
    label = 'TLS'
    create_sub_task(parent=parent_task_key, summary=prefix + ' [apache] - fw test open ports',
                    description='fw test open ports', assignee='favr97v',
                    hours='20', due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [apache] - user lvm dir structure creation',
                    description='prepare directory structure for apache', assignee='favr97v', hours='20',
                    due_date=due_date,
                    components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [apache] - install',
                    description='install 2.4.x apache to webservers',
                    assignee='favr97v', hours='20', due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [apache] - cert requesting, security', description='',
                    assignee='favr97v',
                    hours='20', due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [apache] - config',
                    description=' configure applications, domains, vhosts in webserver', assignee='favr97v',
                    hours='20',
                    due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [wildfly] - fw test open ports',
                    description='fw test open ports, especially for domain controller, jgroup clustering',
                    assignee='favr97v',
                    hours='20', due_date=due_date, components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [wildfly] - user lvm dir structure creation',
                    description='prepare directory structure for apache', assignee='favr97v', hours='20',
                    due_date=due_date,
                    components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [wildfly] - deploy application server',
                    description='create dir structure for instances, links,', assignee='favr97v', hours='20',
                    due_date=due_date,
                    components=component, label=label)
    create_sub_task(parent=parent_task_key, summary=prefix + ' [wildfly] - deploy application',
                    description='deploy application ear, config ant other application specialities',
                    assignee='favr97v', hours='20',
                    due_date=due_date, components=component, label=label)


# reassign_issues_jql("parent = TASK-7481", "favr97v")
# update_issues_status_jql("parent = TASK-7481", ACTION_CLOSE, "Closed.")
# update_issues_parent_jql('project=Tasks AND "Fast Track ID" ~ FT-1371 AND status not in (Closed, Review)', "FT-1607")
# convert_sub_task_to_task(jira.issue('TASK-2700'))
# convert_task_to_sub_task(jira.issue('TASK-2779'), 'TASK-7003')

# convert_task_to_sub_task(jira.issue('TASK-2061'), 'TASK-11364')
# convert_task_to_sub_task(jira.issue('TASK-2211'), 'TASK-11364')
# convert_task_to_sub_task(jira.issue('TASK-2212'), 'TASK-11373')

# Release ID definitions
# DE_EMX_18_04 = '12450'
# for issue in search_sub_tasks('TASK-8695'):
#     set_fix_version(issue, DE_EMX_18_04)

# close_issue(jira.issue('TASK-7314'), 'Done')

# update_issues_status_jql('"Fast Track ID" = FT-860 and status = review', ACTION_CLOSE, "Moved to PROD.")


# MOP UAT - TASK-11230
# move_sub_task(jira.issue('TASK-6970'), 'TASK-11230')
# move_sub_task(jira.issue('TASK-7425'), 'TASK-11230')

# update_parent(jira.issue('TASK-7279'), 'FT-3473')
# update_parent(jira.issue('TASK-8293'), 'FT-3473')
# update_parent(jira.issue('TASK-7108'), 'FT-3473')
# update_parent(jira.issue('TASK-7114'), 'FT-3473')
# update_parent(jira.issue('TASK-7181'), 'FT-3473')
# update_parent(jira.issue('TASK-7223'), 'FT-3473')
# update_parent(jira.issue('TASK-7257'), 'FT-3473')
# update_parent(jira.issue('TASK-7259'), 'FT-3473')
# update_parent(jira.issue('TASK-7281'), 'FT-3473')
# update_parent(jira.issue('TASK-7283'), 'FT-3473')
# update_parent(jira.issue('TASK-7678'), 'FT-3473')
# update_parent(jira.issue('TASK-8157'), 'FT-3473')
# update_parent(jira.issue('TASK-12202'), 'FT-3473')
# update_parent(jira.issue('TASK-17012'), 'FT-3473')
# update_parent(jira.issue('TASK-7246'), 'FT-3473')
# update_parent(jira.issue('TASK-7227'), 'FT-3473')
# update_parent(jira.issue('TASK-7226'), 'FT-3473')

# MOP SIT - TASK-11229
# update_parent(jira.issue(''), 'TASK-11229')
convert_task_to_sub_task(jira.issue('TASK-8066'), 'TASK-7281')

# MOP Wildfly preps TASK-11256
# move_sub_task(jira.issue('TASK-2524'), 'TASK-11256')
# move_sub_task(jira.issue('TASK-2528'), 'TASK-11256')




# create_task(summary='[MISC Infrastructure] - Nessus scan', description='Request Nessus scan, resolve reported issues',
#             assignee='tsku1290', due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=24,
#             fast_track_key='FT-460', label='TLS')
# create_task(summary='[MISC Wildfly] - App server configuration', description='App server configuration',
#             assignee='favr97v', due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=60,
#             fast_track_key='FT-460', label='TLS')
#
# create_task(summary='[MISC Automation]', description='MISC Automation',
#             assignee='favr97v', due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=40,
#             fast_track_key='FT-460', label='TLS')
# create_automation_sub_tasks('[MISC automation] - ', 'TASK-11325', '30.4.2018', 'eSolutions.MISC.Germany')
#
# build_task = create_task(summary='[MISC SIT] - Build SIT', description='Build SIT', assignee='favr97v',
#                          due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=40,
#                          fast_track_key='FT-460', label='TLS')
# create_server_setup_sub_tasks(build_task.key, '[MISC SIT]', '30.4.2018', 'eSolutions.MISC.Germany')
#
# build_task = create_task(summary='[MISC UAT] - Build UAT', description='Build UAT', assignee='favr97v',
#                          due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=40,
#                          fast_track_key='FT-460', label='TLS')
# create_server_setup_sub_tasks(build_task.key, '[MISC UAT]', '30.4.2018', 'eSolutions.MISC.Germany')
#
# build_task = create_task(summary='[MISC PROD] - Build PROD', description='Build PROD', assignee='favr97v',
#                          due_date='2018-04-30', component='eSolutions.MISC.Germany', hours=40,
#                          fast_track_key='FT-460', label='TLS')
# create_server_setup_sub_tasks(build_task.key, '[MISC PROD]', '30.4.2018', 'eSolutions.MISC.Germany')


# create_task(summary='[AWIS] Application migration to WildFly', description='Build PROD', assignee='tsku1290',
#             due_date='2018-05-30', component='eSolutions.WSV.Germany', hours=40,
#             fast_track_key='FT-458', label='TLS')
# create_task(summary='[DWH online] Application migration to WildFly', description='Build PROD', assignee='tsku1290',
#             due_date='2018-05-30', component='eSolutions.WSV.Germany', hours=40,
#             fast_track_key='FT-458', label='TLS')



due_date = '30.6.2018'
component = 'eSolutions.WSV.Germany'
label = 'TLS'


# WSV UAT build subtask creation
# create_sub_task(parent='TASK-11340',
#                 summary='[WSV UAT] Update old WSV to call BSServices on web server VIPa',
#                 description='TLS 1.0 and 1.1 will be disabled on LB. Old apps need to access BS Services by directly accessing web server VIPA. DNS name to IP translation needs to be put into /etc/hosts',
#                 assignee='favr97v',
#                 hours='20',
#                 due_date=due_date,
#                 components=component,
#                 label=label)

# WSV PROD build subtask creation
# create_sub_task(parent='TASK-11349',
#                 summary='[WSV PROD] Update old WSV to call BSServices on web server VIPa',
#                 description='TLS 1.0 and 1.1 will be disabled on LB. Old apps need to access BS Services by directly accessing web server VIPA. DNS name to IP translation needs to be put into /etc/hosts',
#                 assignee='favr97v',
#                 hours='20',
#                 due_date=due_date,
#                 components=component,
#                 label=label)

# update_due_dates_jql("parent =TASK-6831", "30.10.2018")
# update_due_date(jira.issue('TASK-6832'), '13.10.2018')

