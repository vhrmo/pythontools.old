ewasComponents = [
    "eSolutions.eWAS.ADV.Germany",
    "eSolutions.eWAS.Archive.Germany",
    "eSolutions.eWAS.CardControl.Germany",
    "eSolutions.eWAS.DPS.Germany",
    "eSolutions.eWAS.GANS.Germany",
    "eSolutions.eWAS.Germany",
    "eSolutions.eWAS.HSM.Germany",
    "eSolutions.eWAS.Schufa.Germany",
    "eSolutions.UCOM.Germany",
    "eSolutions.AWIS.Germany",
]


wsvComponents = [
    "eSolutions.AMOS.Germany",
    "eSolutions.AppGuard.Germany",
    "eSolutions.Benutzerverwaltung.Germany",
    "eSolutions.Class-Online.Germany",
    "eSolutions.CurrencyCalculator.Germany",
    "eSolutions.docFlow.Germany",
    "eSolutions.EDP.Germany",
    "eSolutions.ESP.Germany",
    "eSolutions.FDBATCH.Germany",
    "eSolutions.GANS-Online.Germany",
    "eSolutions.GTROnline.Germany",
    "eSolutions.Merchant Online Portal.Germany",
    "eSolutions.MISC.Germany",
    "eSolutions.Mule ESB.Germany",
    "eSolutions.PaymentMonitoring.Germany",
    "eSolutions.SepaWebGui.Germany",
    "eSolutions.Tokenizer.Germany",
    "eSolutions.WebLogin.Germany",
    "eSolutions.WSV.Germany",
]

