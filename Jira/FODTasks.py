import JiraTools

tasks = [

    # ['MOP', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    # ['MOP', '70000035', 'Cookie Security: Overly Broad Path (4)'],
    # ['MOP', '70000038', 'Cross-Site Scripting: DOM (33)'],
    # ['MOP', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (11)'],
    # ['MOP', '70000032', 'Header Manipulation: Cookies (2)'],
    # ['MOP', '70000017', 'Log Forging (14)'],
    # ['MOP', '70000001', 'Null Dereference (8)'],
    # ['MOP', '70000114', 'Often Misused: Spring Remote Service (11)'],
    # ['MOP', '70000000', 'Open Redirect (5)'],
    # ['MOP', '70000012', 'Password Management: Hardcoded Password (3)'],
    # ['MOP', '70000018', 'Password Management: Password in Configuration File (6)'],
    # ['MOP', '70000028', 'Portability Flaw: Locale Dependent Comparison (7)'],
    # ['MOP', '70000003', 'Privacy Violation (8)'],
    # ['MOP', '70000013', 'Race Condition: Singleton Member Field (2)'],
    # ['MOP', '70000009', 'Unreleased Resource: Streams (3)'],
    # ['MOP', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    # ['GANS', '70000102', 'ClassLoader Manipulation: Struts 1 (1)'],
    # ['GANS', '70000038', 'Cross-Site Scripting: DOM (1)'],
    # ['GANS', '70000002', 'Cross-Site Scripting: Reflected (424)'],
    # ['GANS', '70000116', 'File Disclosure: Struts (2)'],
    # ['GANS', '70000034', 'Insecure Transport: Mail Transmission (1)'],
    # ['GANS', '70000057', 'Key Management: Empty Encryption Key (3)'],
    # ['GANS', '70000025', 'Key Management: Hardcoded Encryption Key (1)'],
    # ['GANS', '70000017', 'Log Forging (231)'],
    # ['GANS', '70000001', 'Null Dereference (60)'],
    # ['GANS', '70000000', 'Open Redirect (1)'],
    # ['GANS', '70000012', 'Password Management: Hardcoded Password (1)'],
    # ['GANS', '70000018', 'Password Management: Password in Configuration File (10)'],
    # ['GANS', '70000033', 'Path Manipulation (22)'],
    # ['GANS', '70000028', 'Portability Flaw: Locale Dependent Comparison (13)'],
    # ['GANS', '70000003', 'Privacy Violation (555)'],
    # ['GANS', '70000013', 'Race Condition: Singleton Member Field (119)'],
    # ['GANS', '70000027', 'Server-Side Request Forgery (22)'],
    # ['GANS', '70000039', 'SQL Injection (10)'],
    # ['GANS', '70000031', 'System Information Leak: External (205)'],
    # ['GANS', '70000004', 'Unreleased Resource: Database (18)'],
    # ['GANS', '70000009', 'Unreleased Resource: Streams (24)'],
    # ['GANS', '70000019', 'XML External Entity Injection (1)'],
    # ['SepaWebGui', '70000038', 'Cross-Site Scripting: DOM (4)'],
    # ['SepaWebGui', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (2)'],
    # ['SepaWebGui', '70000015', 'Insecure Transport (1)'],
    # ['SepaWebGui', '70000017', 'Log Forging (19)'],
    # ['SepaWebGui', '70000001', 'Null Dereference (646)'],
    # ['SepaWebGui', '70000018', 'Password Management: Password in Configuration File (14)'],
    # ['SepaWebGui', '70000028', 'Portability Flaw: Locale Dependent Comparison (24)'],
    # ['SepaWebGui', '70000003', 'Privacy Violation (256)'],
    # ['SepaWebGui', '70000031', 'System Information Leak: External (38)'],
    # ['SepaWebGui', '70000004', 'Unreleased Resource: Database (30)'],
    # ['SepaWebGui', '70000062', 'Unreleased Resource: Files (3)'],
    # ['SepaWebGui', '70000014', 'Unreleased Resource: Sockets (1)'],
    # ['SepaWebGui', '70000009', 'Unreleased Resource: Streams (65)'],
    # ['SepaWebGui', '70000040', 'Weak Encryption (3)'],
    # ['SepaWebGui', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    # ['SepaWebGui', '70000043', 'Weak SecurityManager Check: Overridable Method (8)'],
    # ['WebLogin', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    # ['WebLogin', '70000002', 'Cross-Site Scripting: Reflected (15)'],
    # ['WebLogin', '70000057', 'Key Management: Empty Encryption Key (3)'],
    # ['WebLogin', '70000017', 'Log Forging (63)'],
    # ['WebLogin', '70000001', 'Null Dereference (83)'],
    # ['WebLogin', '70000000', 'Open Redirect (11)'],
    # ['WebLogin', '70000012', 'Password Management: Hardcoded Password (12)'],
    # ['WebLogin', '70000033', 'Path Manipulation (1)'],
    # ['WebLogin', '70000028', 'Portability Flaw: Locale Dependent Comparison (14)'],
    # ['WebLogin', '70000003', 'Privacy Violation (54)'],
    # ['WebLogin', '70000004', 'Unreleased Resource: Database (36)'],
    # ['WebLogin', '70000009', 'Unreleased Resource: Streams (12)'],
    # ['eWas', '70000024', 'Dynamic Code Evaluation: Unsafe Deserialization (28)'],
    # ['eWas', '70000015', 'Insecure Transport (2)'],
    # ['eWas', '70000034', 'Insecure Transport: Mail Transmission (2)'],
    # ['eWas', '70000021', 'JSON Injection (2)'],
    # ['eWas', '70000017', 'Log Forging (11)'],
    # ['eWas', '70000036', 'Mass Assignment: Insecure Binder Configuration (4)'],
    # ['eWas', '70000068', 'Mass Assignment: Request Parameters Bound into Persisted Objects (1)'],
    # ['eWas', '70000001', 'Null Dereference (6)'],
    # ['eWas', '70000018', 'Password Management: Password in Configuration File (80)'],
    # ['eWas', '70000041', 'Portability Flaw: File Separator (1)'],
    # ['eWas', '70000028', 'Portability Flaw: Locale Dependent Comparison (4)'],
    # ['eWas', '70000003', 'Privacy Violation (21)'],
    # ['eWas', '70000013', 'Race Condition: Singleton Member Field (4)'],
    # ['eWas', '70000027', 'Server-Side Request Forgery (1)'],
    # ['eWas', '70000086', 'Spring Boot Misconfiguration: Actuator Endpoint Security Disabled (16)'],
    # ['eWas', '70000014', 'Unreleased Resource: Sockets (2)'],
    # ['eWas', '70000009', 'Unreleased Resource: Streams (5)'],
    # ['eWas', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    # ['eWas', '70000043', 'Weak SecurityManager Check: Overridable Method (2)'],
    # ['eWas', '70000019', 'XML External Entity Injection (48)'],
    # ['ESP', '70000102', 'ClassLoader Manipulation: Struts 1 (1)'],
    # ['ESP', '70000011', 'Cookie Security: Cookie not Sent Over SSL (2)'],
    # ['ESP', '70000035', 'Cookie Security: Overly Broad Path (2)'],
    # ['ESP', '70000002', 'Cross-Site Scripting: Reflected (1)'],
    # ['ESP', '70000034', 'Insecure Transport: Mail Transmission (2)'],
    # ['ESP', '70000017', 'Log Forging (29)'],
    # ['ESP', '70000001', 'Null Dereference (72)'],
    # ['ESP', '70000000', 'Open Redirect (2)'],
    # ['ESP', '70000012', 'Password Management: Hardcoded Password (1)'],
    # ['ESP', '70000028', 'Portability Flaw: Locale Dependent Comparison (3)'],
    # ['ESP', '70000003', 'Privacy Violation (2)'],
    # ['ESP', '70000031', 'System Information Leak: External (1)'],
    # ['ESP', '70000004', 'Unreleased Resource: Database (3)'],
    # ['ESP', '70000014', 'Unreleased Resource: Sockets (1)'],
    # ['ESP', '70000009', 'Unreleased Resource: Streams (26)'],
    # ['ESP', '70000019', 'XML External Entity Injection (6)'],

    ['AppGuard', '70000038', 'Cross-Site Scripting: DOM (5)'],
    ['AppGuard', '70000002', 'Cross-Site Scripting: Reflected (159)'],
    ['AppGuard', '70000022', 'Denial of Service: Regular Expression (8)'],
    ['AppGuard', '70000010', 'Dynamic Code Evaluation: Code Injection (2)'],
    ['AppGuard', '70000006', 'Header Manipulation (1)'],
    ['AppGuard', '70000001', 'Null Dereference (1)'],
    ['AppGuard', '70000000', 'Open Redirect (93)'],
    ['AppGuard', '70000018', 'Password Management: Password in Configuration File (45)'],
    ['AppGuard', '70000085', 'Password Management: Password in HTML Form (4)'],
    ['AppGuard', '70000028', 'Portability Flaw: Locale Dependent Comparison (8)'],
    ['AppGuard', '70000003', 'Privacy Violation (33)'],
    ['AppGuard', '70000044', 'Privacy Violation: Autocomplete (11)'],
    ['AppGuard', '70000013', 'Race Condition: Singleton Member Field (12)'],
    ['AppGuard', '70000027', 'Server-Side Request Forgery (6)'],
    ['AppGuard', '70000031', 'System Information Leak: External (40)'],
    ['AppGuard', '70000009', 'Unreleased Resource: Streams (7)'],
    ['AppGuard', '70000008', 'Weak Encryption: Insecure Mode of Operation (1)'],
    ['AppGuard', '70000043', 'Weak SecurityManager Check: Overridable Method (1)'],
]

translate = {
    'MOP': {
        'task': 'TASK-7480',
        # 'task': 'TASK-7501',  # in test
        'component': 'eSolutions.Merchant Online Portal.Germany',
        'appId': '87484'
    },
    'AppGuard': {
        'task': 'TASK-7481',
        'component': 'eSolutions.AppGuard.Germany',
        'appId': '87481'
    },
    'ESP': {
        'task': 'TASK-7482',
        'component': 'eSolutions.ESP.Germany',
        'appId': '87478'
    },
    'GANS': {
        'task': 'TASK-7484',
        'component': 'eSolutions.GANS-Online.Germany',
        'appId': '87477'
    },
    'eWas': {
        'task': 'TASK-7487',
        'component': 'eSolutions.eWAS.Germany',
        'appId': '87475'
    },
    'WebLogin': {
        'task': 'TASK-7485',
        'component': 'eSolutions.WebLogin.Germany',
        'appId': '87476'
    },
    'SepaWebGui': {
        'task': 'TASK-7486',
        'component': 'eSolutions.SepaWebGui.Germany',
        'appId': '87483'
    },
}


token = JiraTools.get_atl_token()
for task in tasks:
    app = task[0]
    group = task[1]
    desc = task[2]
    desc_url = "Group:  https://ams.fortify.com/Applications/"+translate[app]['appId']+"/Issues?t=-5105&d="+group+"\n\n"+\
               "Use search if group link is broken:\n"+\
               "Search: https://ams.fortify.com/Applications/"+translate[app]['appId']+"/Issues?t=-5105&g=1000006&srch="+desc[:-4].replace(" ", "%20")

    JiraTools.create_sub_task(
        translate[app]['task'],
        desc,  # summary
        desc_url,  # description
        'faujnmq',  # assignee
        '40',  # hours
        "15.12.2017",  # due date
        translate[app]['component'],  # component
        'FOD',  # label
        token
    )


