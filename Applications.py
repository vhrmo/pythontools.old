import Servers

apps = {
    "UAID-03939": {
        'name': "AppGuard",
        'server_clusters': {'AppGuard'},
        'server_filter': {}
    },

    'UAID-E0167': {
        'name': 'Weblogin',
        'server_clusters': {'WSV', 'MOP'},
        'server_filter': {'WSV-SIT', 'App server', 'JBoss App server', 'Internal Web server', 'External Web server'}
    },

    'UAID-E0168': {
        'name': 'Benutzerverwaltung (BV)',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'JBoss App server', 'Internal Web server'}
    },
    'UAID-E0075': {
        'name': 'AWIS',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'JBoss App server', 'Internal Web server'}
    },
    'UAID-E0065': {
        'name': 'AMOS',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'JBoss App server', 'Internal Web server'}
    },
    'UAID-E0078': {
        'name': 'CLASS-Online',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'JBoss App server', 'Internal Web server'}
    },
    'UAID-E0077': {
        'name': 'GANS-Online',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'JBoss App server', 'Internal Web server', 'External Web server'}
    },
    'UAID-E0067': {
        'name': 'EDP',
        'server_clusters': {'WSV', 'MOP'},
        'server_filter': {'WSV-SIT', 'MOP-SIT', 'JBoss App server', 'Internal Web server', 'External Web server'}
    },
    'UAID-E0066': {
        'name': 'ESP',
        'server_clusters': {'WSV', 'MOP'},
        'server_filter': {'WSV-SIT', 'MOP-SIT', 'JBoss App server', 'Internal Web server', 'External Web server'}
    },
    'UAID-04428': {
        'name': 'GTR Online',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'SpringBoot App server', 'Internal Web server'}
    },
    'UAID-04025': {
        'name': 'SEPA Payment monitoring',
        'server_clusters': {'WSV'},
        'server_filter': {'WSV-SIT', 'SpringBoot App server', 'Internal Web server'}
    },

    'UAID-03113': {
        'name': 'SEPA Mandate management',
        'server_clusters': {'MISC'},
        'server_filter': {}
    },
    'UAID-03116': {
        'name': 'CurrencyCalculator',
        'server_clusters': {'MISC'},
        'server_filter': {}
    },

    'UAID-02590': {
        'name': 'MOP',
        'server_clusters': {'MOP'},
        'server_filter': {}
    },

    'UAID-E0282': {
        'name': 'eWas',
        'server_clusters': {'eWas'},
        'server_filter': {}
    },

    'UAID-E0072': {
        'name': 'FD Batch',
        'server_clusters': {'FD Batch'},
        'server_filter': {}
    },

    'UAID-04091': {
        'name': 'XML Converter',
        'server_clusters': {'FD Batch'},
        'server_filter': {}
    },

    'UAID-E0104': {
        'name': 'Kufo Web',
        'server_clusters': {'Telecash'},
        'server_filter': {}
    },

    'UAID-E0049': {
        'name': 'PolControl',
        'server_clusters': {'Telecash'},
        'server_filter': {}
    },
}


# Return a dictionary of servers for an application - key is server name, values are server descriptions
def get_server_dict(uaid):
    app_info = apps[uaid]
    server_clusters = app_info['server_clusters']
    server_filter = app_info['server_filter']

    if len(server_filter) == 0:
        server_filter = server_clusters

    # print 'Application: ' + app_info['name']
    servers = dict()

    for s in Servers.servers:
        server_name = s[0]
        server_desc = s[1]

        if any(s1 in server_desc for s1 in server_clusters) and any(s2 in server_desc for s2 in server_filter):
            # print s
            servers[server_name.lower()] = server_desc

    return servers


def get_app_uaid(app_name):
    return app_to_uaid[app_name]


# Dictionary comprehension - read https://www.datacamp.com/community/tutorials/python-dictionary-comprehension
app_to_uaid = {apps[i]['name']: i for i in apps.keys()}
# print app_to_uaid