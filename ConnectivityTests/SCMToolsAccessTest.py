from TelnetTest import telnet_test

telnet_test("gerrit.1dc.com", 29418, "Gerrit - 29418")
telnet_test("gerrit.1dc.com", 8443, "Gerrit - 8443")

telnet_test("d4pvir1002.1dc.com", 443, "Gerrit Prod - 443")
telnet_test("d4pvir1002.1dc.com", 29418, "Gerrit Prod - 29418")
telnet_test("d5pvir1002.1dc.com", 443, "Gerrit Prod DR - 443")
# telnet_test("d5pvir1002.1dc.com", 29418, "Gerrit Prod DR - 29418")

telnet_test("jenkins.1dc.com", 443, "Jenkins - 443")
telnet_test("jenkins.1dc.com", 8443, "Jenkins - 8443")
# telnet_test("n4pvir021.1dc.com", 443, "Rundeck Dev - 443")
telnet_test("n4pvir021.1dc.com", 8443, "Rundeck Dev - 8443")
telnet_test("10.75.128.78", 443, "JIRA SDLC - 443")
telnet_test("10.75.128.78", 8443, "JIRA SDLC - 8443")

telnet_test("d4pvir1007.1dc.com", 443, "Rundeck Prod 443")
telnet_test("d5pvir1007.1dc.com", 443, "Rundeck Prod DR 443")
telnet_test("d4pvir1006.1dc.com", 443, "Jenkins Prod 443")
telnet_test("d5pvir1006.1dc.com", 443, "Jenkins Prod DR 443")
telnet_test("d4pvir1001.1dc.com", 443, "Nexus Prod 443")
telnet_test("d5pvir1001.1dc.com", 443, "Nexus Prod DR 443")
telnet_test("10.75.1.225", 443, "JIRA 443")
telnet_test("10.75.1.230", 443, "Confluence 443")

telnet_test("nexus.1dc.com", 8081, "Nexus - 8081")
telnet_test("nexus-emea.1dc.com", 15001, "Nexus - 15001")
telnet_test("nexus-emea.1dc.com", 15101, "Nexus - 15101")
telnet_test("nexus-emea.1dc.com", 16001, "Nexus - 16001")
telnet_test("nexus-emea.1dc.com", 16101, "Nexus - 16101")


telnet_test("portal-eu.firstdata.com", 443, "VDI access")
telnet_test("d4pptl1002.1dc.com", 8000, "SPLUNK")

