from TelnetTest import telnet_test

urls = [
    "online.bcs.firstdata.de",
    "token-pr.1dc.com",
    "token-dr.1dc.com",
    "fortify.1dc.com",
    "token.1dc.com",

    "online.firstdata.com",
    "online-esh.firstdata.com",
    "online-equ.firstdata.com",

    "accountmanager.firstdatams.com",
    "aibmope.firstdata.eu",
    "insight.aibms.com",
    "merchantportal.firstdata.at",

    "cat-sso.firstdata.com",
    "dr-aibmope.firstdata.eu",
    "dr-merchantportal.firstdata.at",
    "dr-mope.firstdata.eu",
    "dr-wsv-int-online.1dc.com",

    "esp.cardprocess.de",

    "misc.firstdata.eu",
    "misc-dr.firstdata.eu",
    "misc-u-i.firstdata.eu",
    "misc-u-i-dr.firstdata.eu",

    "mop.firstdata.eu",
    "mop.privatbank1891.com",
    "mop.privatbank1891.firstdata.eu",
    "mop.telecash.de",
    "mop-dr.privatbank1891.firstdata.eu",
    "mope.firstdata.eu",
    "mop-i.1dc.com",
    "mopq.firstdata.eu",

    "online.bcs.firstdata.de",
    "online.fdi.1dc.com",
    "online.vis.firstdata.de",
    "online-erp.firstdata.de",

    "online.firstdata.de",
    "online.telecash.de",
    "online-cat.telecash.de",
    "online-dr.firstdata.de",
    "online-dr.telecash.de",
    "online-i.firstdata.de",
    "online-qa.firstdata.de",

    "prod-wsv-int-online.1dc.com",
    "sso.firstdata.com",
    "sso-dr.firstdata.com",

    "token1.1dc.com",
    "token2.1dc.com",
    "tokenq1.1dc.com",
    "tokenq2.1dc.com",

    "wsqa.gzs.de",

    "sso.1dc.com",
    "sso-dr.1dc.com",
    "mop-i.1dc.com",
    "mopq.firstdata.eu",
    "online-dev.firstdata.de",

    # "170.186.247.232",
    # "217.73.33.104",
    # "217.73.33.55",
    # "217.73.36.113",
    # "217.73.36.114",
    # "217.73.36.119",
    # "217.73.36.17",
    # "217.73.36.19",
    # "217.73.36.45",
    # "217.73.36.47",
    # "217.73.36.51",
    # "217.73.36.54",
    # "217.73.36.81",
    # "217.73.36.82",
    # "217.73.36.83",
    # "217.73.36.87",
    # "217.73.36.89",
    # "217.73.36.92",
    # "217.73.36.93",
    # "217.73.38.119",
    # "217.73.38.17",
    # "217.73.38.19",
    # "217.73.38.81",
    # "217.73.38.87",
    # "217.73.38.92",
    # "217.73.38.93",
    # "217.73.38.48",
    # "217.73.38.59",
    # "217.73.38.47",




]

for _server in urls:
    telnet_test(_server, 443)

# telnet_test('10.72.68.46', 443)
