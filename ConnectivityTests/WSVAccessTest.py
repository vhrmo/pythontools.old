from TelnetTest import telnet_test

urls = [

    # UAT web servers
    ["10.72.66.164", "N5CVWB995 (R5CVWB1004) WSV-UAT-External Web server"],
    # ["R5CVWB1004-vipa01", "N5CVWB995 (R5CVWB1004) WSV-UAT-External Web server"],
    # ["R5CVWB1003-vipa01", "N5CVWB996 (R5CVWB1003) WSV-UAT-External Web server"],
    # ["R5CVWB1006-vipa01", "N5CVWB993 (R5CVWB1006) WSV-UAT-Internal WEB server"],
    # ["R5CVWB1005-vipa01", "N5CVWB994 (R5CVWB1005) WSV-UAT-Internal WEB server"],
]

for _server in urls:
    telnet_test(_server[0], 443, _server[1])

