from TelnetTest import telnet_test

dbs = [

    # ["N5DVAP992", "Tokenizer DEV"],
    # ["a5dvvo110", "Tokenizer SIT"],
    # ["oraidbq2.firstdata.de", "Tokenizer QA - oraidbq2"],
    # ["oraidbq4.firstdata.de", "Tokenizer QA - oraidbq4"],

    ["a4qvvo120.1dc.com", "EPSUAT_HA"],
    ["10.68.47.46", "EPSUAT_HA"],
    ["10.68.47.47", "EPSUAT_HA"],
    ["10.68.47.48", "EPSUAT_HA"],

]

for _server in dbs:
    telnet_test(_server[0], 1521, _server[1])

