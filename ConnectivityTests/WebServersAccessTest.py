from TelnetTest import telnet_test


servers = [
    "10.68.37.111",
    "10.68.4.104",
    "10.68.4.105",
    "10.68.5.202",
    "10.68.5.203",
    "10.68.6.205",
    "10.68.7.50",
    "10.68.7.51",
    "10.68.7.53",
    "10.77.17.112",
    "10.77.17.113",
    "10.77.17.147",
    "10.77.17.170",
    "10.77.17.238",
    "10.77.17.239",
    "10.77.18.112",
    "10.77.18.113",
    "10.77.37.111",
    "10.77.4.67",
    "10.77.4.68",
    "10.77.49.103",
    "10.77.5.140",
    "10.77.5.141",
    "10.77.5.154",
    "10.77.5.155",
    "10.77.5.46",
    "10.77.5.47",
    "10.77.5.50",
    "10.77.6.49",
]

for s in servers:
    telnet_test(s, 443)


