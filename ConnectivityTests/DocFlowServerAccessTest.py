from TelnetTest import telnet_test


servers = [
    ["w4qvap988", "docFlow-UAT-App server"],
    ["w4qvap990", "docFlow-UAT-File server"],

    ["w4pvap189", "docFlow-PROD-Primary-App server"],
    ["w4pvap190", "docFlow-PROD-Primary-App server"],
    ["w4pvap191", "docFlow-PROD-Primary-App server"],
    ["w4pvap192", "docFlow-PROD-Primary-App server"],
    ["w4pvap209", "docFlow-PROD-Primary-File server"],
    ["w5bvap1014", "docFlow-PROD-DR-App server"],
    ["w5bvap1015", "docFlow-PROD-DR-App server"],
    ["w5bvap1016", "docFlow-PROD-DR-App server"],
    ["w5bvap1017", "docFlow-PROD-DR-App server"],
]


for s in servers:
    telnet_test(s[0], 3389, s[1])
