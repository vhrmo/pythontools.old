import re
import socket
import urllib2
import urlparse

from bs4 import BeautifulSoup


# list of Hobbit start pages to start the search
start_urls = [
              # 'http://n4pvir017/hobbit/LinuxProd/LinuxProd.html',
              # 'http://n4pvir017/hobbit/LinuxDR/LinuxDR.html',
              # 'http://n4pvir017/hobbit/LinuxUAT/LinuxUAT.html',
              # 'http://n4pvir017/hobbit/UnixProd/UnixProd.html',
              # 'http://n4pvir017/hobbit/UnixDR/UnixDR.html',
              # 'http://n4pvir017/hobbit/UnixUAT/UnixUAT.html',
              'http://n4pvir017/hobbit/LinuxTest/LinuxTest.html',
              'http://n4pvir017/hobbit/UnixTest/UnixTest.html'
]

# list applications we are interested in
apps = [
    'WSV',
    'SepaWebApp-Prod',
    'eWas',
    'TC_WEB',
    'SEPA',
    'TC-Prod',
    'SEPA-2-kufo',
    'MOP',
    'SSO_Openam',
    'kufoBatch',
    'MOP-Tokenizer',
    'TC-Prod',
    'eDoks',
    'SepaWebApp',
    'TC-Test'','
    'SEPA-CAT',
]


def parse(_start_url):
    # print 'Parsing ', _start_url
    soup = BeautifulSoup(urllib2.urlopen(_start_url).read(), "html.parser")
    tables = soup.find_all('table')

    for row in tables[1].select('tr'):
        try:
            tds = row.find_all('td')
            if len(tds) > 1:
                parse_app_cluster(_start_url, tds[0], tds[1])
            if len(tds) > 3:
                parse_app_cluster(_start_url, tds[3], tds[4])

        except:
            print row
            raise


def parse_app_cluster(_start_url, td1, td2):
    app = td1.getText(" ", strip=True)
    # print '    ', app # use this to print all application names
    if app in apps:
        try:
            # get link to the application cluster's hobbit page
            link = urlparse.urljoin(_start_url, td2.center.a.get('href'))
            # print '    ', app, ':', link

            # parse the application cluster page - get server names
            soup2 = BeautifulSoup(urllib2.urlopen(link).read(), "html.parser")
            server_regex = re.compile("HOST=(\w*)")
            for lnk in soup2.find_all('a', href=re.compile("SERVICE=procs")):
                # print lnk
                lnk = lnk.get('href')
                match = server_regex.search(lnk)
                server_name = match.group(1)

                try:
                    ip = socket.gethostbyname(server_name)
                except:
                    ip = 'Unknown'

                # print socket.gethostbyaddr("69.59.196.211")
                print start_url, '|', link, '|', app, '|', server_name, '|', ip


        except:
            print 'td2:', td2
            print 'lnk:', lnk
            raise


# go through all start pages in Hobbit and look for applications we are interested in
for start_url in start_urls:
    parse(start_url)

