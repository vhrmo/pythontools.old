import glob
import fnmatch
import os
from setuptools import archive_util
import shutil
import gzip


path = "c:/a - Web server logs/Old WSV/apache_logs_wsqa/apache/fdd/gz"
# path = "c:/a - Web server logs/WSV/UAT/WSV UAT - n5cvwb993-access/fdc/logs/apache/bi.uat.firstdata.com/*.gz"


print shutil.get_archive_formats()

for root, dirnames, filenames in os.walk(path):
    for filename in fnmatch.filter(filenames, '*.gz'):
        fn = os.path.join(root, filename)
        print fn

        with gzip.open(fn, 'rb') as f_in:
            with open(fn[:-3], 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)



