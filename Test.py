import datetime
import re
import pprint
pp = pprint.PrettyPrinter(indent=4)

# from ua_parser import user_agent_parser
# ua_string = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET4.0C; .NET4.0E; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MS-RTC LM 8; InfoPath.3)'
# parsed_string = user_agent_parser.Parse(ua_string)
# pp.pprint(parsed_string)

from user_agents import parse
# iPhone's user agent string
ua_string = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET4.0C; .NET4.0E; .NET CLR 3.5.30729; .NET CLR 3.0.30729; MS-RTC LM 8; InfoPath.3)'
user_agent = parse(ua_string)
print(user_agent)

print datetime.datetime.today().strftime('%Y-%m-%d')


t = 'Multistar High Capacity 3S 5200MAh Multi-Rotor Lipo Pack;Weight : 436g | IN STOCK;EU15.29 was EU28.61;'


cells_regex = re.compile(" (\d)S ")
capacity_regex = re.compile(" (\d*)[mahMAH]* ")
weight_regex = re.compile(" (\d*)g ")
price_regex = re.compile("EU(\d*.\d*) ")

match = capacity_regex.search(t)
print match.group(1)
match = cells_regex.search(t)
print match.group(1)
match = weight_regex.search(t)
print match.group(1)
match = price_regex.search(t)
print match.group(1)


server_info = re.compile("Apache\s*$|\s*$")

m = server_info.match("Apache  2 ")
print m

m = server_info.match("")
print m

server_info = re.compile("max-age=\d*; includeSubdomains")

m = server_info.match("max-age=10886400; includeSubdomains")
print m

m = server_info.match("")
print m

import time
print time.ctime()

a = "John"
print "{:<15}".format(a) + "a"
print "        {:<15}{}".format(a, 'b')


# regex = re.compile("<a href=/Applications/\d+/Issues\?d=(\d+). title=.([^\"])*")
regex = re.compile('<a href="/Applications/\d+/Issues\?d=(\d+)" title="([^"]*)"', re.MULTILINE)

m = regex.findall('http\n'
                '<a href="/Applications/87484/Issues?d=70000025" title="Cookie Security: Cookie not Sent Over SSL (2)" class="truncate">'
                '<a href="/Applications/87484/Issues?d=7064654025" title="Cookie Security: Cookie not Sent Over SSL (2)" class="truncate">'
                ''
                ''
                ''
                '')
print m

# // %20
appid = '87484'
src='Password Management: Password in Configuration File (6)'
print "https://ams.fortify.com/Applications/"+appid+"/Issues?t=-5105&g=1000006&srch="+src[:-4].replace(" ", "%20")+"\nbla"


