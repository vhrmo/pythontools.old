# Generates a report of the GANS online groups defined in the groups.properties file
import json

aggregate_groups = {}
groups = {}
screens = {}

# ssh = ssh_cache.get(scan_server)
# if ssh is not None:

def processAggregateGroupLine(key, value):
    aggregate_group, nr = key.split('.')
    g = aggregate_groups.get(aggregate_group)
    if g is None:
        aggregate_groups[aggregate_group] = []
    aggregate_groups[aggregate_group].append(value)


def processGroup(key, value):
    try:
        action = None
        if '/' in key:
            tmp, action = key.split('/')
            group, screen = tmp.split('.')
        else:
            group, screen = key.split('.')

        s = screens.get(screen)
        if s is None:
            screens[screen] = {
                'groups': {}
            }

        g = screens[screen]['groups'].get(group)
        if g is None:
            screens[screen]['groups'][group] = {
                'actions': []
            }
        if action:
            screens[screen]['groups'][group]['actions'].append(action)

    except:
        print key
        print value
        raise


def processLine(aLine):
    key, value = aLine.split('=')
    if '.#' in key:
        # aggregated group
        processAggregateGroupLine(key, value)
    else:
        processGroup(key, value)


with open("groups.properties") as aFile:
    for aLine in aFile:
        aLine = aLine.strip()
        if len(aLine)>0 and aLine[0] != '#':
            processLine(aLine)


# print aggregate_groups
# print json.dumps(aggregate_groups, indent=2, sort_keys=True)

print screens
print json.dumps(screens, indent=2, sort_keys=True)
