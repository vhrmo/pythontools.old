import re
import sys
import json
import requests
import logging
import Servers
import Secrets
import Applications
from requests.packages import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning


def get_appmap_server_dict(uaid):
    json_data = session.get('https://echo.1dc.com/api/v1/app_infrastructure/page/'+uaid+'/?format=json').json()
    # print json.dumps(json_data)

    # Dictionary comprehension - read https://www.datacamp.com/community/tutorials/python-dictionary-comprehension
    new_dict = {i['name'].lower(): i['environment'] for i in json_data['result'] if i['infra_type'] == 'server'}
    return new_dict


def reconcile_appmap_infrastructure_info(uaid, show_matches=False):
    app_info = Applications.apps[uaid]
    print 'Application:', app_info['name'], 'https://echo.1dc.com/appmap/'+uaid+'/Infrastructure/'

    appmap_servers = get_appmap_server_dict(uaid)
    servers = Applications.get_server_dict(uaid)

    incorrect = servers.viewkeys() ^ appmap_servers.viewkeys()

    if show_matches:
        correct = servers.viewkeys() & appmap_servers.viewkeys()
        print '    Matching, correctly listed servers: '
        for i in correct:
            print '        {:<10} - {}'.format(i, servers[i])

    missing_in_appmap = incorrect & servers.viewkeys()
    extra_in_appmap = incorrect & appmap_servers.viewkeys()
    if len(missing_in_appmap) > 0:
        print '    Missing in AppMap, please add them: '
        for i in missing_in_appmap:
            print '        {:<10} - {}'.format(i, servers[i])
            # print '        ' + i + servers[i]
    if len(extra_in_appmap) > 0:
        print '    To be removed from AppMap: '
        for srvr in extra_in_appmap:
            print '        {:<10} - {}'.format(srvr, Servers.get_server_desc(srvr))


def reconcile_appmap_infrastructure_info_all_apps(show_matches=False):
    for uaid in Applications.apps:
        reconcile_appmap_infrastructure_info(uaid, show_matches)


urllib3.disable_warnings(InsecureRequestWarning)
session = requests.Session()

payload = {"user_name": Secrets.user_name, "password": Secrets.pwd}
r = session.post('https://echo.1dc.com/login/', json=payload, verify=False)
print "Authentication:", r.status_code

# reconcile_appmap_infrastructure_info_all_apps()

# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('Benutzerverwaltung (BV)'), show_matches=True)
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('Weblogin'), show_matches=True)
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('AppGuard'), show_matches=True)

# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('EDP'), show_matches=True)
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('ESP'), show_matches=True)
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('MOP'), show_matches=True)
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('GTR Online'))
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('CLASS-Online'), show_matches=True)

# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('FD Batch'))
# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('XML Converter'))

# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('Kufo Web'), show_matches=True)
reconcile_appmap_infrastructure_info(Applications.get_app_uaid('PolControl'), show_matches=True)

# reconcile_appmap_infrastructure_info(Applications.get_app_uaid('eWas'))

