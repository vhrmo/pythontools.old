import sys
import socket

import Servers


def get_ip(dns_name):
    try:
        ip = socket.gethostbyname(dns_name)
        return ip
    except:
        return None


def multiline_ping(server_name, server_desc):
    ip = get_ip(server_name)
    print server_name + ";" + server_desc + ";" + ip + ";" + server_name

    for s in [(server_name + "-vipa0" + str(l + 1)) for l in range(9)]:
        ip = get_ip(s)
        if ip is not None:
            print server_name + ";" + server_desc + ";" + ip + ";" + s


def ping(_server):
    try:
        ip = socket.gethostbyname(_server)
        sys.stdout.write(ip)
        sys.stdout.write(';')

    except:
        sys.stdout.write(';')


def single_line_ping(server_name):
    sys.stdout.write(server_name)
    sys.stdout.write(';')
    ping(server_name)
    ping(server_name + '-vipa01')
    ping(server_name + '-vipa02')
    ping(server_name + '-vipa03')
    ping(server_name + '-vipa04')
    ping(server_name + '-vipa05')
    ping(server_name + '-vipa06')
    ping(server_name + '-vipa07')
    ping(server_name + '-vipa08')
    ping(server_name + '-vipa09')
    ping(server_name + '-vipa10')
    ping(server_name + '-vipa11')
    ping(server_name + '-vipa12')
    ping(server_name + '-vipa13')
    ping(server_name + '-vipa14')
    sys.stdout.flush()
    print


for server in Servers.servers:
    server_name = server[0]
    server_desc = server[1]

    if "Internal Web" in server_desc :
        # single_line_ping(server_name)
        multiline_ping(server_name, server_desc)
