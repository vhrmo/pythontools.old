import re
import sys
import urllib2
import httplib  # or http.client if you're on Python 3
import Secrets

from bs4 import BeautifulSoup, Comment


# list of pages to parse
start_urls = [
    # 'http://www.hobbyking.com/hobbyking/store/__1388__85__Batteries_Accessories-MultiStar_Batteries_Multi_Rotor_.html',
    # 'http://www.hobbyking.com/hobbyking/store/uh_listCategoriesAndProducts.asp?cwhl=XX&pc=85&idCategory=86&curPage=1&v=&sortlist=&sortMotor=&LiPoConfig=',

    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=1',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=2',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=3',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=4',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=5',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=6',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=7',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=8',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=9',
    'https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=10',

]


def search(reg_ex, text):
    match = reg_ex.search(text)
    if match:
        return match.group(1)
    else:
        return "Error parsing: " + text


def parse(_start_url):
    # print 'Parsing ', _start_url
    # print urllib2.urlopen(_start_url).read()
    response = urllib2.urlopen(_start_url)

    # item_regexp = re.compile("name\":\"([~a-zA-Z0-9 ]*)\".*price\":([\d.]*)")
    item_regexp = re.compile("name\":\"([^\"]+)")
    price_regexp = re.compile("\"price\":([\d.]+)")
    weight_regex = re.compile("dimension12\":(\d+)")
    store_regex = re.compile("dimension17\":\"([^\"]+)")

    cells_regex = re.compile("(\d+)[sS]")
    capacity_regex = re.compile("(\d+)[mM][aA][hH]")

    # print "Name; Cells; Capacity; Weight; Price; Availability; Store; URL; Line"

    for line in response:
        if "oro_gtm.regProduct(" in line:
            name = search(item_regexp, line)
            price = search(price_regexp, line)
            price = price.replace('.', ',') # for excel to accept this as number
            weight = search(weight_regex, line)
            store = search(store_regex, line)
            if "ingle Cell" in name or "ingle cell" in name:
                cells = "1"
            else:
                cells = search(cells_regex, name)
            if "Out of stock" in line:
                availability = "Out of stock"
            else:
                availability = "In stock"
            capacity = search(capacity_regex, name)
            print name + ";" + cells + ";" + capacity + ";" + weight + ";" + price + ";" + availability + ";" + store + ";" + _start_url + ";" + line


def parse_old(_start_url):
    # print 'Parsing ', _start_url
    print urllib2.urlopen(_start_url).read()
    soup = BeautifulSoup(urllib2.urlopen(_start_url).read(), "html.parser")

    cells_regex = re.compile(" (\d)*[sS]")
    capacity_regex = re.compile(" (\d*)[mahMAH]* ")
    weight_regex = re.compile(" (\d*)g ")
    price_regex = re.compile("EU(\d*.\d*)")
    single_cell_regex = re.compile("single cell")

    for comment in soup.findAll(text=lambda text: isinstance(text, Comment)):
        try:
            if comment in [' item name ']:
                item_name = comment.next_element.next_element.getText(" ", strip=True)
                sys.stdout.write(item_name)
                sys.stdout.write(';')

                try:
                    match = cells_regex.search(item_name)
                    sys.stdout.write(match.group(1))
                    sys.stdout.write(';')
                except:
                    if "single cell" in item_name:
                        sys.stdout.write("1;")
                    else:
                        sys.stdout.write("?;")


                match = capacity_regex.search(item_name)
                sys.stdout.write(match.group(1))
                sys.stdout.write(';')

                sys.stdout.flush()

            if comment in [' weight and stock ']:
                weight = comment.next_element.next_element.getText(" ", strip=True)
                match = weight_regex.search(weight)
                sys.stdout.write(match.group(1))
                sys.stdout.write(';')

                sys.stdout.flush()
            if comment in [' prices and buy button ']:
                price = comment.next_element.next_element.getText(" ", strip=True)
                match = price_regex.search(price)
                sys.stdout.write(match.group(1))
                sys.stdout.flush()
                print

        except:
            print

    # for row in tables[1].select('tr'):
    #     try:
    #         tds = row.find_all('td')
    #         if len(tds) > 1:
    #             parse_app_cluster(_start_url, tds[0], tds[1])
    #         if len(tds) > 3:
    #             parse_app_cluster(_start_url, tds[3], tds[4])
    #
    #     except:
    #         print row
    #         raise


urllib2.install_opener(
    urllib2.build_opener(
        urllib2.ProxyHandler({'https': Secrets.proxy_pwd})
    )
)


httplib._MAXHEADERS = 1000

for page in range(1,36):
    # parse('https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=' + str(page))
    parse('https://hobbyking.com/en_us/batteries/lipo-batteries-all-brands.html?wrh=7&p=' + str(page))

# parse('https://hobbyking.com/en_us/batteries/lipoly-all-brands.html?wrh=7&p=15')

# for start_url in start_urls:
#     parse(start_url)

