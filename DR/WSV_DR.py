import DR

internal_web_sites = [
    "online.1dc.com",
    'bs.1dc.com',
    "online.firstdata.de",
    "online.vis.firstdata.de",
    "online.bcs.firstdata.de",
    "online.fdi.1dc.com",
    "bi.1dc.com",
]

external_web_sites = [
    "online.firstdata.com",
    "online.firstdata.de",
    "bi.firstdata.com",
]

http_test_internal_urls = [
    "https://online.1dc.com/",
    "https://online.1dc.com/login",
    "https://online.1dc.com/amos",
    "https://online.1dc.com/class",
    "https://online.1dc.com/esp",
    "https://online.1dc.com/edp",
    "https://online.1dc.com/gans",
    "https://online.1dc.com/bv",

    "https://bs.1dc.com/BSServices",

    "https://online.firstdata.de/login",
    "https://online.firstdata.de/dwhmis/distribution",
    "https://online.firstdata.de/webADV/webADVWeb",

    "https://bi.1dc.com/login",

]

http_test_external_urls = [
    "https://online.firstdata.com",
    "https://online.firstdata.com/login",
    "https://online.firstdata.com/gans",
    "https://online.firstdata.com/edp",
    "https://online.firstdata.com/esp",

    "https://online.firstdata.de",
    "https://online.firstdata.de/login",
    "https://online.firstdata.de/dwhmis/distribution",
    "https://online.firstdata.de/ews",

    "https://bi.firstdata.com/login",
]


DR.run_dr_monitor('wsv_dr_test.out', internal_web_sites, http_test_internal_urls, 10)
# DR.run_dr_monitor('wsv_dr_test.out', external_web_sites, http_test_external_urls, 10)
