import datetime
import os
import socket
import urllib2
import time
import platform


def ping(server):
    r = {
        'time': datetime.datetime.now(),
        'server': server,
        'test-type': 'dns-lookup'
    }

    try:
        ip = socket.gethostbyname(server)
        r['ip'] = ip
    except:
        r['ip'] = 'Error'

    return r


def http_test(url):
    r = {
        'time': datetime.datetime.now(),
        'url': url,
        'test-type': 'http-test '
    }

    try:
        resp = urllib2.urlopen(url)
        r['http-response-code'] = resp.getcode()
    except urllib2.HTTPError as e:
        r['http-response-code'] = e.code
        r['msg'] = e.msg
    except:
        r['http-response-code'] = 'Error'

    return r


def format_ping_data(out):
    return "{:%d.%m.%Y %H:%M:%S}   {:<18} {:<18}".format(out['time'], out['ip'], out['server'])


def format_http_test_data(out):
    return "{:%d.%m.%Y %H:%M:%S}   {:<18} {:<18}".format(out['time'], out['http-response-code'], out['url'])


def run_dr_monitor(fname, server_list, http_list, interval_seconds):
    ips = dict()

    # get a list of initial IPs
    for _server in server_list:
        out = ping(_server)
        str = format_ping_data(out)
        print 'Initial IP:   ' + str
        ips[out['server']] = out['ip']

    while True:

        f = open(fname, 'a')

        if platform.system() == 'Windows':
            os.popen('ipconfig /flushdns >/dev/null 2>&1')

        for _server in server_list:
            out = ping(_server)
            str = format_ping_data(out)

            if ips.get(out['server']) != out['ip']:
                print 'IP change :   ' + str
                ips[out['server']] = out['ip']

            f.write(out['test-type'] + '  ' + str + '\n')

        for _url in http_list:
            out = http_test(_url)
            str = format_http_test_data(out)
            f.write(out['test-type'] + '  ' + str + '\n')

        f.close()
        time.sleep(interval_seconds)
