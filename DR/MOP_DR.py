import DR

internal_web_sites = [
    "mop.firstdata.eu",
    "token.1dc.com"
]

external_web_sites = [
    "mop.firstdata.eu",
    "insight.aibms.com",
    "mop.telecash.de",
    "accountmanager.firstdatams.com",
    "merchantportal.firstdata.at",
    "mop.privatbank1891.com"
]

http_test_internal_urls = [
    "https://mop.firstdata.eu/",
    "https://mop.firstdata.eu/login",
    # "https://mop.firstdata.eu/esp/", # this is causing issue due to redirect
    "https://mop.firstdata.eu/edp",
    "https://mop.firstdata.eu/mop/faces/login/aibms/login.xhtml?acquirer=aibms&language=en",
    "https://mop.firstdata.eu/mop/faces/login/fdi/login.xhtml?acquirer=fdi&language=en",
    "https://mop.firstdata.eu/mop/faces/login/fdms/login.xhtml?acquirer=fdms&language=en",
    "https://mop.firstdata.eu/mop/faces/login/tc/login.xhtml?acquirer=tc&language=en",
    "https://mop.firstdata.eu/mop/faces/login/fda/login.xhtml?acquirer=fda&language=en",
    "https://token.1dc.com/",
    "https://token.1dc.com/login"
]

http_test_external_urls = [
    'https://insight.aibms.com',
    'https://insight.aibms.com/mop/faces/login/aibms/login.xhtml?acquirer=aibms&language=en',
    'https://mop.telecash.de',
    'https://mop.telecash.de/mop/faces/login/tc/login.xhtml?acquirer=tc&language=en',
    'https://accountmanager.firstdatams.com/',
    'https://merchantportal.firstdata.at/',
]

DR.run_dr_monitor('mop_dr_test.out', internal_web_sites, http_test_internal_urls, 10)
# DR.run_dr_monitor('mop_dr_test.out', external_web_sites, http_test_external_urls, 10)

