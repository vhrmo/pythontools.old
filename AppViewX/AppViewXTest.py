import base64
import json

import requests
import urllib3
from requests.packages.urllib3.exceptions import InsecureRequestWarning

import Secrets

urllib3.disable_warnings(InsecureRequestWarning)
s = requests.Session()

headers = {
    'Referer': 'login.do',
}

vips = {}


def login():
    payload = {"username": Secrets.user_name, "password": base64.b64encode(Secrets.pwd)}
    r = s.post("https://appviewx.1dc.com/AppViewXNGWeb/authenticate.do", json=payload, headers=headers)
    print "Authentication:", r.status_code
    # print r.text


def findVip(vip):
    payload = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                # "mandatory": true
            },
            {
                "labelName": "ENTER VIP Address",
                "values": vip,
                "fieldId": "txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                # "mandatory": true
            }
        ],
        "workflowName": "LB - Modify Load balancing VIP",
        "scriptName": "find_vip",
        "aclFilter": "None",
        "requestCategory": "default"
    }

    r = s.post('https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do', json=payload,
               headers=headers)
    # print "findVip:", r.status_code
    json = r.json()

    return {
        "vip": vip,
        "vip_name": json["response"][0]["txt_vip"],
        "lb_ip": json["response"][0]["txt_lb_unit"]
    }


def getVipInfo(vip):
    vipInfo = vips.get(vip)
    if vipInfo is not None:
        # print "conn from cache"
        return vipInfo
    else:
        vipInfo = findVip(vip)
        vips[vip] = vipInfo
        return vipInfo


def get_tls_config(vip):
    vipInfo = getVipInfo(vip)
    payload = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "Load Balancer IP",
                "values": vipInfo["lb_ip"],
                "fieldId": "txt_lb_unit",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": 'true'
            },
            {
                "labelName": "VIP NAME",
                "values": vipInfo["vip_name"],
                "fieldId": "txt_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": 'true'
            },
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": 'true'
            },
            {
                "labelName": "ENTER VIP Address",
                "values": vip,
                "fieldId": "txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": 'true'
            },

        ],
        "workflowName": "LB - Modify Load balancing VIP",
        "scriptName": "get_tls_config",
        "aclFilter": "None",
        # "flowName": null,
        "requestCategory": "default"
    }

    r = s.post('https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do', json=payload,
               headers=headers)
    # print "Response:", r.status_code
    json = r.json()
    # {u'httpStatusCode': 200, u'response': [{u'txt_vip_tls_config': u'Virtual IP Current TLS Configuration:\nSSLv 3: DISABLED\nTLS 1.0: DISABLED\nTLS 1.1: DISABLED\nTLS 1.2:ENABLED'}]}
    vipInfo["vip_tls_config"] = json["response"][0]["txt_vip_tls_config"]
    return vipInfo


def get_server_pool(vip):
    vipInfo = getVipInfo(vip)
    payload = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "SELECT OUTPUT",
                "values": "Server Pool Members",
                "fieldId": "mgt_sel_output",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": "",
            },
            {
                "labelName": "UNIT IP",
                "values": vipInfo["lb_ip"],
                "fieldId": "mgt_txt_lb_unit",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
            },
            {
                "labelName": "VIP TYPE",
                "values": "LOAD BALANCER",
                "fieldId": "mgt_txt_vip_type",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
            },
            {
                "labelName": "VIP NAME",
                "values": vipInfo["vip_name"],
                "fieldId": "mgt_txt_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
            },
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "mgt_txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
            },
            {
                "labelName": "ENTER VIP Address",
                "values": vip,
                "fieldId": "mgt_txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
            },
            {
                "fieldId": "tab_svg_b_members",
                "labelName": "DR Server Pool",
                "elementType": "Tabular",
                "values": [],
                "group": "REQUEST DETAILS"
            },
            {
                "fieldId": "tab_svg_a_members",
                "labelName": "PRIMARY Server Pool",
                "elementType": "Tabular",
                "values": [],
                "group": "REQUEST DETAILS"
            },
            {
                "labelName": "SELECT TYPE OF REQUEST",
                "values": "Manage VIP",
                "fieldId": "dd_req_type",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": "",
            }
        ],
        "workflowName": "LB - Load Balancing Request",
        "scriptName": "vip_output",
        "aclFilter": "None",
        "requestCategory": "default"
    }

    r = s.post('https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do', json=payload,
               headers=headers)
    # print "Response:", r.status_code
    json = r.json()
    vipInfo["server_pool"] = json["response"][0]["mgt_vip_deatils"]
    return vipInfo


def get_monitor_settings(vip):
    vipInfo = getVipInfo(vip)
    payload = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "SELECT OUTPUT",
                "values": "Server Monitor Settings",
                "fieldId": "mgt_sel_output",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": ""

            },
            {
                "labelName": "UNIT IP",
                "values": vipInfo["lb_ip"],
                "fieldId": "mgt_txt_lb_unit",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""

            },
            {
                "labelName": "VIP TYPE",
                "values": "LOAD BALANCER",
                "fieldId": "mgt_txt_vip_type",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""

            },
            {
                "labelName": "VIP NAME",
                "values": vipInfo["vip_name"],
                "fieldId": "mgt_txt_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""

            },
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "mgt_txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""

            },
            {
                "labelName": "ENTER VIP Address",
                "values": vip,
                "fieldId": "mgt_txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""

            },
            {
                "labelName": "SELECT TYPE OF REQUEST",
                "values": "Manage VIP",
                "fieldId": "dd_req_type",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": ""

            }

        ],
        "workflowName": "LB - Load Balancing Request",
        "scriptName": "vip_output",
        "aclFilter": "None",
        "requestCategory": "default"
    }

    r = s.post('https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do', json=payload,
               headers=headers)
    # print "Response:", r.status_code
    json = r.json()
    vipInfo["monitor_setting"] = json["response"][0]["mgt_vip_deatils"]
    return vipInfo


def get_full_vip_info(vip):
    vipInfo = getVipInfo(vip)
    payload = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "SELECT OUTPUT",
                "values": "FULL VIP Config",
                "fieldId": "mgt_sel_output",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": ""
            },
            {
                "labelName": "UNIT IP",
                "values": vipInfo["lb_ip"],
                "fieldId": "mgt_txt_lb_unit",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""
            },
            {
                "labelName": "VIP TYPE",
                "values": "LOAD BALANCER",
                "fieldId": "mgt_txt_vip_type",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""
            },
            {
                "labelName": "VIP NAME",
                "values": vipInfo["vip_name"],
                "fieldId": "mgt_txt_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""
            },
            {
                "labelName": " FIND VIP",
                "values": "",
                "fieldId": "mgt_btn_find_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Button",
                "parent": ""
            },
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "mgt_txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""
            },
            {
                "labelName": "ENTER VIP Address",
                "values": vip,
                "fieldId": "mgt_txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": ""
            }

        ],
        "workflowName": "LB - Load Balancing Request",
        "scriptName": "vip_output",
        "aclFilter": "None",
        "requestCategory": "default"
    }

    r = s.post('https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do', json=payload,
               headers=headers)
    # print "Response:", r.status_code
    json = r.json()
    vipInfo["full_vip_info"] = json["response"][0]["mgt_vip_deatils"]
    return vipInfo


def loadVipInfo(vip):
    try:
        findVip(vip)
    except Exception as e:
        print "Error in findVip:               " + vip
        return

    try:
        get_full_vip_info(vip)
    except Exception as e:
        print "Error loading full vip info:    " + vip

    try:
        get_tls_config(vip)
    except Exception as e:
        print "Error loading TLS info:         " + vip

    try:
        get_monitor_settings(vip)
    except Exception as e:
        print "Error loading monitor settings: " + vip

    try:
        return get_server_pool(vip)
    except Exception as e:
        print "Error loading server pool info: " + vip


ips = [
    '10.74.66.71',
    # '10.74.66.43',
    # '10.72.66.43',
    # '10.74.66.42',
    # '10.74.66.73',
    # '10.72.66.73',
    # '10.74.66.78',
    # '10.72.66.78',
    # '10.74.66.17',
    # '10.72.66.17',
    # '10.74.66.18',
    # '10.72.66.18',
    # '10.74.66.54',
    # '10.72.66.32',
    # '10.74.66.180',
    # '10.72.66.180',
    # '10.74.66.181',
    # '10.72.66.181',
    # '10.68.196.79',
    # '10.77.196.73',
    # '10.74.66.8',
    # '10.72.66.12',
    # '10.68.196.8',
    # '10.77.196.13',
    # '10.74.66.70',
    # '10.72.66.70',
    # '10.74.66.85',
    # '10.72.66.85',
    # '10.74.66.163',
    # '10.72.66.164',
    # '10.74.66.51',
    # '10.72.66.52',
    # '10.74.66.56',
    # '10.72.66.57',
    # '10.68.196.45',
    # '10.77.196.45',
    # '10.68.196.12',
    # '10.77.196.17',
    # '10.68.196.41',
    # '10.77.196.41',
    # '10.68.196.56',
    # '10.77.196.56',
    # '10.68.196.44',
    # '10.77.196.44',
    # '10.68.196.57',
    # '10.77.196.57',
    # '10.68.196.13',
    # '10.77.196.18',
    # '10.72.66.31',
    # '10.72.66.56',
    # '10.77.196.124',
    # '10.77.196.42',
    # '10.77.196.125',
    # '10.77.196.53',
    # '10.77.196.46'
]



login()

for ip in ips:
    loadVipInfo(ip)

print json.dumps(vips, indent=2, sort_keys=True)
