

servers = [
    ["r5dvap1000", "AppGuard-SIT-App server"],
    ["r5dvap1001", "AppGuard-SIT-App server"],
    ["n5dvwb1002", "AppGuard-SIT-Web server"],
    ["n5dvwb1003", "AppGuard-SIT-Web server"],

    ["r4cvap1000", "AppGuard-UAT-App server"],
    ["r4cvap1001", "AppGuard-UAT-App server"],
    ["n4cvwb1010", "AppGuard-UAT-Web server"],
    ["n4cvwb1011", "AppGuard-UAT-Web server"],

    ["r4pvap1007", "AppGuard-PROD-Primary-App server"],
    ["r4pvap1008", "AppGuard-PROD-Primary-App server"],
    ["r5pvap1000", "AppGuard-PROD-DR-App server"],
    ["r5pvap1001", "AppGuard-PROD-DR-App server"],
    ["n4pvwb1010", "AppGuard-PROD-Primary-Web server"],
    ["n4pvwb1011", "AppGuard-PROD-Primary-Web server"],
    ["n5pvwb1008", "AppGuard-PROD-DR-Web server"],
    ["n5pvwb1009", "AppGuard-PROD-DR-Web server"],

    ["n5dvap992", "Tokenizer-DEV-App server"],

    ["a4tvap106", "Tokenizer-UAT-App server"],

    ["a4pvap040", "Tokenizer-PROD-Batch server"],
    ["a5pvap015", "Tokenizer-PROD-DR-Batch server"],
    ["10.68.37.110", "a5fvap002 - Tokenizer-PROD-DR-Batch server"],


    ["n5dvap998", "FD Batch-SIT-Batch server (on eWas)"],
    ["n4cvap998", "FD Batch-UAT-Primary-App server"],

    ["n4pvap1002", "FD Batch-PROD-NotUsedYet-Primary-Batch server"],
    ["n4pvap1003", "FD Batch-PROD-Primary-Batch server"],
    ["n4pvap1004", "FD Batch-PROD-Primary-Batch server"],
    ["n4pvap1029", "FD Batch-PROD-Primary-Batch server"],
    ["n4pvap1030", "FD Batch-PROD-Primary-Batch server"],
    ["n5pvap1002", "FD Batch-PROD-NotUsedYet-DR-Batch server"],
    ["n5pvap1003", "FD Batch-PROD-DR-Batch server"],
    ["n5pvap1004", "FD Batch-PROD-DR-Batch server"],
    ["n5pvap1015", "FD Batch-PROD-DR-Batch server"],
    ["n5pvap1016", "FD Batch-PROD-DR-Batch server"],
    ["r4pvap1049", "FD Batch-PROD-Primary-Batch server"],
    ["r5fvap1002", "FD Batch-PROD-DR-Batch server"],

    ["n5dvap985", "MISC-SIT-App server"],
    ["n5dvwb994", "MISC-SIT-Web server"],

    ["n4cvap1005", "MISC-UAT-Primary-App server"],
    ["n4cvap1006", "MISC-UAT-Primary-App server"],
    ["n5cvap1001", "MISC-UAT-DR-App server"],
    ["n5cvap1002", "MISC-UAT-DR-App server"],
    ["n4cvwb1006", "MISC-UAT-Primary-Web server"],
    ["n4cvwb1007", "MISC-UAT-Primary-Web server"],
    ["n5cvwb1002", "MISC-UAT-DR-Web server"],
    ["n5cvwb1003", "MISC-UAT-DR-Web server"],

    ["n4pvap1007", "MISC-PROD-Primary-App server"],
    ["n4pvap1008", "MISC-PROD-Primary-App server"],
    ["n5pvap1007", "MISC-PROD-DR-App server"],
    ["n5pvap1008", "MISC-PROD-DR-App server"],
    ["n4pvwb1006", "MISC-PROD-Primary-External Web server"],
    ["n4pvwb1007", "MISC-PROD-Primary-External Web server"],
    ["n5pvwb1006", "MISC-PROD-DR-External Web server"],
    ["n5pvwb1007", "MISC-PROD-DR-External Web server"],

    ["n5dvap989", "MOP-SIT-App server"],
    ["r5dvap1009", "MOP-SIT-App server"],
    ["n5dvwb1004", "MOP-SIT-Web server"],
    ["n5dvwb1005", "MOP-SIT-Web server (unused)"],

    ["r4cvap1030", "MOP-UAT-Primary-App server"],
    ["r5cvap1014", "MOP-UAT-DR-App server"],
    ["r4cvap1051", "MOP-UAT-Primary-App server (new)"],
    ["r5cvap1033", "MOP-UAT-DR-App server (new)"],
    ["r4cvwb1011", "MOP-UAT-Primary-Web server"],
    ["r4cvwb1026", "MOP-UAT-Primary-Web server (new)"],
    ["r5cvwb1010", "MOP-UAT-DR-Web server"],
    ["r5cvwb1019", "MOP-UAT-DR-Web server (new)"],

    ["r4pvap1070", "MOP-PROD-Primary-App server"],
    ["r4pvap1121", "MOP-PROD-Primary-App server (new)"],
    ["r5pvap1043", "MOP-PROD-DR-App server"],
    ["r5pvap1098", "MOP-PROD-DR-App server (new)"],
    ["n4pvwb1014", "MOP-PROD-Primary-Internal Web server"],
    ["r4pvwb1047", "MOP-PROD-Primary-Internal Web server (new)"],
    ["r4pvwb1048", "MOP-PROD-Primary-Internal Web server (new)"],
    ["n5pvwb1011", "MOP-PROD-DR-Internal Web server"],
    ["r5pvwb1039", "MOP-PROD-DR-Internal Web server (new)"],
    ["r5pvwb1040", "MOP-PROD-DR-Internal Web server (new)"],
    ["n4pvwb1013", "MOP-PROD-Primary-External Web server"],
    ["r4pvwb1045", "MOP-PROD-Primary-External Web server (new)"],
    ["r4pvwb1046", "MOP-PROD-Primary-External Web server (new)"],
    ["n5pvwb1010", "MOP-PROD-DR-External Web server"],
    ["r5pvwb1037", "MOP-PROD-DR-External Web server (new)"],
    ["r5pvwb1038", "MOP-PROD-DR-External Web server (new)"],

    ["n5dvap988", "WSV-SIT-Unused-App server"],
    ["n5dvap996", "WSV-SIT-App server"],
    ["n5tvap998", "WSV-SIT-App server"],
    ["n5dvwb995", "WSV-SIT-Web server"],
    ["n5dvwb996", "WSV-SIT-Unused-Web server"],

    ["r5cvap1003", "WSV-UAT-JBoss App server"],
    ["r5cvap1004", "WSV-UAT-JBoss App server"],
    ["r5cvap1005", "WSV-UAT-SpringBoot App server"],
    ["r5cvap1006", "WSV-UAT-SpringBoot App server"],
    ["n5cvwb993", "WSV-UAT-Internal Web server"],
    ["r5cvwb1005", "WSV-UAT-Internal Web server"],
    ["n5cvwb995", "WSV-UAT-External Web server"],
    ["r5cvwb1003", "WSV-UAT-External Web server"],

    ["n4pvap016", "WSV-PROD-Primary-JBoss App server"],
    ["n4pvap017", "WSV-PROD-Primary-JBoss App server"],
    ["n5bvap994", "WSV-PROD-DR-JBoss App server"],
    ["n5bvap995", "WSV-PROD-DR-JBoss App server"],
    ["n4pvap018", "WSV-PROD-Primary-SpringBoot App server"],
    ["n4pvap019", "WSV-PROD-Primary-SpringBoot App server"],
    ["n5bvap996", "WSV-PROD-DR-SpringBoot App server"],
    ["n5bvap997", "WSV-PROD-DR-SpringBoot App server"],
    ["n4pvwb011", "WSV-PROD-Primary-Internal Web server"],
    ["n4pvwb012", "WSV-PROD-Primary-Internal Web server"],
    ["n5bvwb992", "WSV-PROD-DR-Internal Web server"],
    ["n5bvwb993", "WSV-PROD-DR-Internal Web server"],
    ["n4pvwb009", "WSV-PROD-Primary-External Web server"],
    ["n4pvwb010", "WSV-PROD-Primary-External Web server"],
    ["n5bvwb994", "WSV-PROD-DR-External Web server"],
    ["n5bvwb995", "WSV-PROD-DR-External Web server"],

    ["n5dvap982", "Telecash-DEV-App server"],
    ["n5dvwb993", "Telecash-DEV-Web server"],
    ["r4tvap1010", "Telecash-DEV-RabbitMQ server"],
    ["r4tvap1011", "Telecash-DEV-RabbitMQ server"],
    ["n5dvap994", "Telecash-DEV-MText server"],
    ["n5dvwb997", "Telecash-DEV-DB server"],
    ["n5qvdb999", "Telecash-UAT-DB server"],
    ["n4qvap997", "Telecash-UAT-App server"],
    ["n4qvwb997", "Telecash-UAT-Web server"],
    ["r4cvap1007", "Telecash-UAT-RabbitMQ server"],
    ["n5qvap994", "Telecash-UAT-MText server"],
    ["n5qvap994", "Telecash-UAT-MText server"],
    ["n4pvap010", "Telecash-PROD-Primary-App server"],
    ["n4pvap040", "Telecash-PROD-Primary-App server"],
    ["n4pvap1034", "Telecash-PROD-Primary-App server"],
    ["n5bvap1004", "Telecash-PROD-DR-App server"],
    ["n5bvap988", "Telecash-PROD-DR-App server"],
    ["n4pvdb002", "Telecash-PROD-Primary-DB server"],
    ["n5pvdb002", "Telecash-PROD-DR-DB server"],
    ["n4pvwb1017", "Telecash-PROD-Primary-Internal Web server"],
    ["n4pvwb1018", "Telecash-PROD-Primary-Internal Web server"],
    ["n5bvwb1002", "Telecash-PROD-DR-Internal Web server"],
    ["n5bvwb1003", "Telecash-PROD-DR-Internal Web server"],
    ["n4pvwb015", "Telecash-PROD-Primary-External Web server"],
    ["n4pvwb1019", "Telecash-PROD-Primary-External Web server"],
    ["n5bvwb1004", "Telecash-PROD-DR-External Web server"],
    ["n5bvwb991", "Telecash-PROD-DR-External Web server"],
    ["r4pvap1090", "Telecash-PROD-RabbitMQ server"],
    ["r4pvap1091", "Telecash-PROD-RabbitMQ server"],
    ["r5pvap1070", "Telecash-PROD-DR-RabbitMQ server"],
    ["r5pvap1071", "Telecash-PROD-DR-RabbitMQ server"],

    ["r5dvwb1000", "eWas new-SIT-Web server"],
    ["r5dvap1010", "eWas new-SIT-App server"],
    ["r4cvwb1012", "eWas new-UAT-Primary-Web server"],
    ["r4cvwb1013", "eWas new-UAT-Primary-Web server"],
    ["r5cvwb1011", "eWas new-UAT-DR-Web server"],
    ["r4cvap1031", "eWas new-UAT-Primary-App server"],
    ["r4cvap1032", "eWas new-UAT-Primary-App server"],
    ["r5cvap1015", "eWas new-UAT-DR-App server"],
    ["r4pvwb1026", "eWas new-PROD-Primary-Web server"],
    ["r4pvwb1027", "eWas new-PROD-Primary-Web server"],
    ["r5pvwb1020", "eWas new-PROD-DR-Web server"],
    ["r5pvwb1021", "eWas new-PROD-DR-Web server"],
    ["r4pvap1074", "eWas new-PROD-Primary-App server"],
    ["r4pvap1075", "eWas new-PROD-Primary-App server"],
    ["r5pvap1046", "eWas new-PROD-DR-App server"],
    ["r5pvap1047", "eWas new-PROD-DR-App server"],

    ["n5dvdb998", "eWas-SIT-DB server"],
    ["n5dvwb998", "eWas-SIT-Web server"],
    ["n5dvap998", "eWas-SIT-App server"],
    ["n4cvap997", "eWas-UAT-Primary-App server"],
    ["n5cvap996", "eWas-UAT-DR-App server"],
    ["n4cvdb998", "eWas-UAT-Primary-DB server"],
    ["n5cvdb997", "eWas-UAT-DR-DB server"],
    ["n4cvwb998", "eWas-UAT-Primary-Web server"],
    ["n4cvwb999", "eWas-UAT-Primary-Web server"],
    ["n5cvwb999", "eWas-UAT-DR-Web server"],
    ["n4cvwb1004", "eWas-UAT-Primary-Internal Web server"],
    ["n4cvwb1005", "eWas-UAT-Primary-Internal Web server"],
    ["n5cvwb1001", "eWas-UAT-DR-Internal Web server"],
    ["n4pvap012", "eWas-PROD-Primary-App server"],
    ["n4pvap013", "eWas-PROD-Primary-App server"],
    ["n5pvap006", "eWas-PROD-DR-App server"],
    ["n5pvap007", "eWas-PROD-DR-App server"],
    ["n4pvdb003", "eWas-PROD-Primary-DB server"],
    ["n5pvdb004", "eWas-PROD-DR-DB server"],
    ["n4pvwb005", "eWas-PROD-Primary-Web server"],
    ["n4pvwb006", "eWas-PROD-Primary-Web server"],
    ["n5pvwb002", "eWas-PROD-DR-Web server"],
    ["n5pvwb003", "eWas-PROD-DR-Web server"],

    ["d4pvir1000", "SaltStack"],
    ["d5pvir1000", "SaltStack DR"],
]


old_unused_servers = [
    ["a4qvap999", "Tokenizer-Unused-Decomisioned-App server"],
    ["a4qvap998", "Tokenizer-Unused-Decomisioned-Batch server"],
    ["a5qvap998", "Tokenizer-Unused-Decomisioned-DR-Batch server"],
    ["a4pvap002", "Tokenizer-Unused-Decomisioned-Batch server"],
    ["a4qvir999", "Tokenizer-Unused-Decomisioned-? I ?"],
    ["a4qvwb999", "Tokenizer-Unused-Decomisioned-Web server"],

    ["n4pvwb1001", "FD Batch-PROD-Unused-Decomisioned-Primary-Web server"],
    ["n4pvwb1002", "FD Batch-PROD-Unused-Decomisioned-Primary-Internal Web server"],
    ["n4pvwb1003", "FD Batch-PROD-Unused-Decomisioned-Primary-Internal Web server"],
    ["n5pvwb1001", "FD Batch-PROD-Unused-Decomisioned-DR-Web server"],
    ["n5pvwb1002", "FD Batch-PROD-Unused-Decomisioned-DR-Internal Web server"],
    ["n5pvwb1003", "FD Batch-PROD-Unused-Decomisioned-DR-Internal Web server"],

    ["a4pvap001", "MOP-PROD-to-be-decomissioned-Primary-App server"],
    ["a5fvap001", "MOP-PROD-to-be-decomissioned-DR-App server"],
    ["a4pvwb001", "MOP-PROD-to-be-decomissioned-Primary-Internal Web server"],
    ["a5fvwb001", "MOP-PROD-to-be-decomissioned-DR-Internal Web server"],
    ["10.77.49.101", "a5qvap999 - MOP-UAT-to-be-decomissioned-App server"],
    ["a5qvwb999", "MOP-UAT-to-be-decomissioned-Web server"],

    ["a4ppwb001", "MOP-PROD-decomissioned-Primary-External Web server"],
    ["a5fpwb001", "MOP-PROD-decomissioned-DR-External Web server"],

    ["wsvapp1", "WSV Old Bad Vilbel!-PROD-App server"],
    ["wswebint1", "WSV Old Bad Vilbel!-PROD-Web server"],
    ["wsqa", "WSV Old Bad Vilbel!-UAT-"],
    ["wsvapp2", "WSV Old Bad Vilbel!-PROD-"],
    ["wswebext1", "WSV Old Bad Vilbel!-PROD-"],

    ["n4cvwb1003", "!KUFO?-UAT-"],
    ["n5cvwb1000", "!KUFO?-UAT-"],
    ["n4cvap1004", "!KUFO?-UAT-"],
    ["n5cvap1000", "!KUFO?-UAT-"],
    ["a4pvap015", "!Unknown server - decomission request sent"],
    ["a4pvwb002", "!Unknown server - decomission request sent"],
    ["a4pvap050", "--GIS server"],
    ["a5qvir999", "a5qvir999 - --Main WPAR (used by NAGE)"],
]


# Dictionary comprehension - read https://www.datacamp.com/community/tutorials/python-dictionary-comprehension
server_to_desc = {i[0]: i[1] for i in servers}
old_unused_server_to_desc = {i[0]: i[1] for i in old_unused_servers}


def get_server_desc(name):
    if name in server_to_desc:
        return server_to_desc[name]
    elif name in old_unused_server_to_desc:
        return old_unused_server_to_desc[name]
    else:
        return ""


def get_servers(cond1, cond2):
    s = [x for x in servers if (cond1 in x[1] and cond2 in x[1])]
    return s

