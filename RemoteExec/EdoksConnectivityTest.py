import socket


def telnet_test(server, port, comment = ""):

    try:
        ip = socket.gethostbyname(server)

    except:
        ip = "Error"

    try:
        sock = socket.socket()
        sock.connect((server, port))
        connTest = 'ok'

    except:
        connTest = 'Error'

    print server, ';', ip, ';', port, ';', connTest, ';', comment


telnet_test("edkpappj-1.gzs.de", 443)
telnet_test("edkpappj-2.gzs.de", 443)
telnet_test("edkpappj-1-dr.gzs.de", 443)
telnet_test("edkpappj-2-dr.gzs.de", 443)



