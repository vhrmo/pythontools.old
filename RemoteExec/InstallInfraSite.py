import os
import shutil
import re
import tarfile

import Secrets
import Users
import paramiko
import Servers

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())


def excludes_fn(name):
    return ".git" in name or ".idea" in name

tar_folder = "c:\\work\\Projects.2\\infra\\"

tar = tarfile.open('archive_name.tgz', 'w:gz')
print os.path.basename(tar_folder)
tar.add(tar_folder, arcname="\\", exclude=excludes_fn)
tar.close()

# MOP QA
# ssh.connect("r4cvwb1011", username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

# WSV SIT
ssh.connect("n5dvwb995", username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

sftp = ssh.open_sftp()
sftp.put("archive_name.tgz", "archive_name.tgz")
sftp.close()

os.remove("archive_name.tgz")

ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("rm -rf /opt/apache/htdocs/infra/*")
ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("tar xf archive_name.tgz -C /opt/apache/htdocs/infra/")
ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("rm archive_name.tgz ")


