import os
import paramiko
import shutil
import fnmatch
import datetime
import gzip
import tarfile

import Secrets
import Servers

LOG_ROOT = 'c:/a__logs/'

# Create folder for logs
LOG_DIR = LOG_ROOT + 'esp-logs-' + datetime.datetime.today().strftime('%Y-%m-%d') + '/'

def createOutputDir():

    if os.path.exists(LOG_DIR):
        shutil.rmtree(LOG_DIR)
    os.mkdir(LOG_DIR)

def downloadLogs():
    createOutputDir()

    for s in Servers.servers:
        server_name = s[0]
        server_desc = s[1]

        if any(s in server_desc for s in ("MOP", "WSV")) \
                and all(s in server_desc for s in ("App", "PROD")) \
                and server_name[0] <> "a" \
                and "SpringBoot" not in server_desc:

            print server_name, ",", server_desc

            try:
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                ssh.connect(server_name, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

                # check for the locked account
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uname")
                if "expired" in ssh_stderr.read():
                    print server_name, ",", server_desc, ", Password change required!!!!!!!!!!!!!!!!!!!"
                else:
                    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('find /fdc/logs -name "ESP_STATEMENT*" | xargs tar --ignore-failed-read -zcf ~/`hostname`-esplogs.tar.gz')
                    print ssh_stderr.read()
                    print ssh_stdout.read()
                    sftp = ssh.open_sftp()
                    sftp.get(server_name + "-esplogs.tar.gz", LOG_DIR + server_name+" - " + server_desc + " - logs.tar.gz")
                    sftp.close()
                    ssh.exec_command("rm ~/*-esplogs.tar.gz")

                ssh.close()
            except Exception as e:
                print e



def extractLogsOld():
    for root, dirnames, filenames in os.walk(LOG_DIR):
        for filename in fnmatch.filter(filenames, '*.gz'):
            fn = os.path.join(root, filename)
            print fn

            with gzip.open(fn, 'rb') as f_in:
                with open(fn[:-3], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)


def extractLogs():
    for root, dirnames, filenames in os.walk(LOG_DIR):
        for filename in fnmatch.filter(filenames, '*.tar.gz'):
            with open(LOG_DIR + filename[:-7]+".csv", 'w') as f_out:  # output file
                with tarfile.open(root + filename) as tar:
                    # tar.extractall(path=LOG_DIR + filename[:-7])
                    for entry in tar:  # list each entry one by one
                        date = entry.name[-10:]  # last 10 chars are the date in file name
                        if date[0] == '2':  # ignore the files without a date in filename - latest log files
                            fileobj = tar.extractfile(entry)
                            for line in fileobj:
                                f_out.write(date + ";")
                                f_out.write(line)




# downloadLogs()
extractLogs()


