import socket
import json

import paramiko

# in some cases this may be changed to other port number for a specific test
import Secrets
import Servers

port = '443'

ips = {}

# AppGuard
appguard_urls = [
    ['10.77.17.238', 'sit-sso.firstdata.com-external'],  # TLS1.2
    ['10.74.66.42', 'cat-sso.firstdata.com-external'],  # TLS1.2
    ['10.74.66.43', 'sso.firstdata.com-external'],
    ['10.72.66.43', 'sso-dr.firstdata.com-external'],
]

# Cognos
cognos_urls = [
    ['10.72.66.56', 'bi.uat.firstdata.com-external'],
    ['10.74.66.56', 'bi.firstdata.com-external'],
    ['10.72.66.57', 'bi-dr.firstdata.com-external'],
    ['10.77.196.46', 'bi.uat.1dc.com-internal'],
    ['10.68.196.56', 'bi.1dc.com-internal'],
    ['10.77.196.56', 'bi-dr.1dc.com-internal'],
]

# eWas
ewas_urls = [
    ['10.74.66.36', 'ewas2u.firstdata.de-external'],
    ['10.72.66.36', 'ewas2u-dr.firstdata.de-external'],
    ['10.74.66.35', 'ewas2.firstdata.de-external'],
    ['10.72.66.35', 'ewas2-dr.firstdata.de-external'],
]

# MISC
misc_urls = [
    ['10.74.66.73', 'misc.firstdata.eu-external'],
    ['10.72.66.73', 'misc-dr.firstdata.eu-external'],
    ['10.74.66.78', 'misc-u-i.firstdata.eu-external'],
    ['10.72.66.78', 'misc-u-i-dr.firstdata.eu-external'],
    ['10.77.17.200', 'misc-i.firstdata.eu-internal'],
]

# Tokenizer
tokenizer_urls = [
    ['10.68.49.105', 'tokenq1.1dc.com-internal - to be removed'],
    ['10.77.49.105', 'tokenq2.1dc.com-internal - to be removed'],
    ['10.68.196.8',  'esh-tokenq-new.1dc.com'],
    ['10.77.196.13', 'eqx-tokenq-new.1dc.com'],

    ['10.68.196.79', 'token-pr.1dc.com'],
    ['10.77.196.73', 'token-dr.1dc.com'],

    ['10.68.37.113', 'token1.1dc.com-internal - to be removed'],
    ['10.77.37.113', 'token2.1dc.com-internal - to be removed'],
]

# Telecash
telecash_urls = [
    ['10.74.66.85', 'barvus-m.telecash.de-external'],
    ['10.72.66.85', 'barvus-m-dr.telecash.de-external'],
    ['10.74.66.70', 'online.telecash.de-external'],
    ['10.72.66.70', 'online-dr.telecash.de-external'],

    ['10.74.66.71', 'online-cat.telecash.de-external'],
    ['10.77.17.230', 'online-i.telecash.1dc.com'], # n5dvwb993-vipa02 - Telecash-DEV-Web server
]

# URL, Internal VIP for the URL
wsv_urls1 = [
    ['10.72.66.51', 'online-qa.firstdata.de-external'],
    ['10.77.196.42', 'online-qa.firstdata.de-internal'],
    ['10.77.196.125', 'bs-qa.firstdata.de-internal'],
    ['10.74.66.163', 'online.firstdata.com-external'],
    ['10.72.66.164', 'online-dr.firstdata.com-external'],
    ]
wsv_urls = [
    ['10.77.17.168', 'online-dev.firstdata.de-internal'],
    ['10.77.17.167', 'online-i.firstdata.de-internal'],
    ['10.77.17.172', 'aps-i.any.firstdata.de-internal'],
    ['10.77.17.173', 'aps-i.demo.firstdata.de-internal'],
    ['10.77.17.174', 'aps-i.ges1.firstdata.de-internal'],
    ['10.77.17.175', 'aps-i.ges2.firstdata.de-internal'],

    ['10.72.66.51', 'online-qa.firstdata.de-external'],
    ['10.77.196.42', 'online-qa.firstdata.de-internal'],
    ['10.77.196.125', 'bs-qa.firstdata.de-internal'],
    ['10.77.196.124', 'online-qa.1dc.com-internal'],

    ['10.74.66.51', 'online.firstdata.de-external'],
    ['10.72.66.52', 'online-dr.firstdata.de-external'],

    ['10.74.66.163', 'online.firstdata.com-external'],
    ['10.72.66.164', 'online-dr.firstdata.com-external'],

    ['10.68.196.41', 'online.firstdata.de-internal'],
    ['10.77.196.41', 'dr-wsv-int-online.1dc.com-internal'],

    ['10.68.196.130', 'bs.firstdata.de-internal'],
    ['10.77.196.130', 'bs.firstdata.de (DR)-internal'],

    ['10.68.196.57', 'online.fdi.1dc.com-internal'],
    ['10.77.196.57', 'online.fdi.1dc.com (DR)-internal'],

    ['10.68.196.44', 'online.bcs.firstdata.de-internal'],
    ['10.77.196.44', 'online.bcs.firstdata.de (DR site)-internal'],

    ['10.68.196.45', 'online.vis.firstdata.de-internal'],
    ['10.77.196.45', 'online.vis.firstdata.de (DR site)-internal'],
]

wsv_stunnel = [
    ["n5bvap994", "online-qa.firstdata.de-internal"],

]

# MOP PROD URLs:
mop_urls = [
    ['10.77.17.180', 'mop-i.1dc.com-internal'],
    ['10.74.68.104', 'mopq.1dc.com-internal'],

    ['10.68.37.112', 'mop.firstdata.eu-internal'],
    ['10.77.37.112', 'mop-dr.firstdata.eu-internal'],

    ['10.74.66.17', 'mope.firstdata.eu-external'],
    ['10.72.66.17', 'dr-mope.firstdata.eu-external'],

    ['10.74.66.18', 'insight.aibms.com_aibmope.firstdata.eu-external'],
    ['10.72.66.18', 'dr-aibmope.firstdata.eu-external'],

    ['10.74.68.79', 'accountmanager-vipa.1dc.com-external'],
    ['10.72.68.46', 'dr-accountmanager-vipa.1dc.com-external'],

    ['10.74.68.43', 'mop.telecash.de-external'],
    ['10.72.68.47', 'mop-dr.telecash.de-external'],

    ['10.74.68.44', 'merchantportal.firstdata.at-external'],
    ['10.72.68.48', 'dr-merchantportal.firstdata.at-external'],

    ['10.74.68.20', 'mop.privatbank1891.com-external'],
    ['10.72.68.20', 'mop-dr.privatbank1891.firstdata.eu-external'],

]


# Blocking call to ssh.exec_command
def exec_command(ssh, cmd):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
    exit_status = ssh_stdout.channel.recv_exit_status()
    if exit_status != 0:
        print("Error running command:", cmd)

    return ssh_stdout


def produce_full_ssl_report(ssh, ip, desc, only_protocols=False):
    print 'Scanning ' + desc
    fname = desc + ('_only_protocols' if only_protocols else '')
    # script -q -c "./testssl.sh $1" $1.ansi
    exec_command(ssh, 'script -q -c "./testssl.sh ' + (
        '--protocols ' if only_protocols else '') + ip + '" ' + fname + '.ansi')
    # cat $1.ansi | ./ansi2html.sh > $1.html
    exec_command(ssh, 'cat ' + fname + '.ansi | ./ansi2html.sh > ' + fname + '.html')

    if only_protocols:
        print "========================================================================================================"
        print exec_command(ssh, 'cat ' + fname + '.ansi').read()
        print "========================================================================================================"

    sftp = ssh.open_sftp()
    sftp.get(fname + '.ansi', "c:/a__testssl/" + fname + '.ansi')
    sftp.get(fname + '.html', "c:/a__testssl/" + fname + '.html')
    sftp.close()


def distribute_script():
    for testSrvr in TEST_SERVERS:
        prefix = testSrvr[0]
        serverName = testSrvr[1]

        ssh = connect_scan_server(prefix+"bla", serverName)

        exec_command(ssh, "rm -rf testssl.sh")

        sftp = ssh.open_sftp()
        sftp.put("c:/work/Soft/cygwin64/home/tsku1290/testssl.sh", "testssl.sh")
        sftp.put("c:/work/Soft/cygwin64/home/tsku1290/etc+doc.tar", "etc+doc.tar")
        sftp.close()

        exec_command(ssh, "chmod a+x testssl.sh")
        exec_command(ssh, "tar xf etc+doc.tar")

        print serverName + " done"


TEST_SERVERS = [
    ["10.72.",      "r5pvwb1037"],        # MOP-PROD-DR-External Web server (new) OpenSSL 1.0.2k-fips  26 Jan 2017
    ["10.74.",      "r4pvwb1045"],        # MOP-PROD-Primary-External Web server (new) OpenSSL 1.0.2k-fips  26 Jan 2017
    ["10.77.16.",   "r5dvap1000"],        # AppGuard-SIT-App server  OpenSSL 1.0.1e-fips 11 Feb 2013
    ["10.77.17.",   "r5dvap1000"],        # AppGuard-SIT-App server  OpenSSL 1.0.1e-fips 11 Feb 2013
    ["10.77.37.",   "a5pvap015"],         # Tokenizer-PROD-DR-Batch server  OpenSSL 1.0.2j  26 Sep 2016
    ["10.77.",      "r5cvwb1019"],        # MOP-UAT-DR-Web server (new) OpenSSL 1.0.2k-fips  26 Jan 2017
    ["10.68.37.",   "a4tvap106"],         # Tokenizer-UAT-App server OpenSSL 1.0.2h  3 May 2016
    ["10.68.",      "r4cvwb1026"],        # MOP-UAT-Primary-Web server(new) OpenSSL 1.0.2k-fips  26 Jan 2017
]


def select_scan_server(ip, desc):
    for testSrvr in TEST_SERVERS:
        prefix = testSrvr[0]
        serverName = testSrvr[1]
        if prefix in ip:
            return serverName

    print "ERROR - Test server not defined for " + ip + " - " + desc
    return None


ssh_cache = {}


def connect_scan_server(ip, desc):
    scan_server = select_scan_server(ip, desc)
    if scan_server is None:
        return None

    ssh = ssh_cache.get(scan_server)
    if ssh is not None:
        # print "conn from cache"
        return ssh

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(scan_server, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

    # check for the locked account
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uname")
    if "expired" in ssh_stderr.read():
        print scan_server + " - Password change required!!!!!!!!!!!!!!!!!!!"
        ssh.close()
        return None
    else:
        ssh_cache[scan_server] = ssh
        return ssh


def full_scan(urls):
    for r in urls:
        desc = r[1]
        ip = r[0]

        try:
            ssh = connect_scan_server(ip, desc)
            if ssh is not None:
                produce_full_ssl_report(ssh, ip, desc, only_protocols=False)
                # ssh.close()
        except:
            # print server_name, ",", server_desc, ", Errorrrrrrrrrrrrrrrrrrrrrrr"
            raise


def telnet_test(ip, p, desc):
    global port
    port = p
    ssh = connect_scan_server(ip, desc)
    if ssh is not None:
        _telnet_test(ssh, ip, desc)


def _telnet_test(ssh, ip, desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('echo exit | timeout 2 telnet ' + ip + ' ' + port)
    exit_status = ssh_stdout.channel.recv_exit_status()
    # print exit_status
    # print ssh_stderr.read()
    if exit_status < 2:
        if 'Connection refused' in ssh_stderr.read():
            print "Connection refused on port", port, " -", desc
            return False
        else:
            return True
    elif exit_status == 127:
        # Command not found exit code - cannot check connectivity - proceed to next step ...
        return True
    else:
        print "No listener on port", port, "        -", desc
        return False


stat_supported = {
    '-ssl2': 'Very Bad',
    '-ssl3': 'Very Bad',
    '-tls1': 'Bad',
    '-tls1_1': 'Bad',
    '-tls1_2': 'OK'
}

stat_not_supported = {
    '-ssl2': 'OK',
    '-ssl3': 'OK',
    '-tls1': 'OK',
    '-tls1_1': 'OK',
    '-tls1_2': 'Bad'
}


def quick_ssl_test(ssh, ip, desc, option):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(
        'echo Q | timeout 2 openssl s_client -connect ' + ip + ':' + port + ' ' + option)
    exit_status = ssh_stdout.channel.recv_exit_status()

    # check if command not found exit status - timeout is not available on all servers, try withuout it
    if exit_status == 127:
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(
            'echo Q | openssl s_client -connect ' + ip + ':' + port + ' ' + option)
        exit_status = ssh_stdout.channel.recv_exit_status()

    if exit_status == 0:
        print desc, 'Supported       -', stat_supported[option]
        ips[ip][option] = "ENABLED"
    else:
        print desc, "Not supported   -", stat_not_supported[option]
        ips[ip][option] = "DISABLED"


def quick_scan_ip(ip, desc):
    ssh = connect_scan_server(ip, desc)
    # first check if anything is listening on target port
    if _telnet_test(ssh, ip, desc):
        print "==========================================================="
        print 'Scanning ' + desc + ' VIP: ' + ip
        print "-----------------------------------------------------------"
        # -ssl2, -ssl3, -tls1, -tls1_1, or -tls1_2
        ips[ip] = {}
        quick_ssl_test(ssh, ip, 'SSLv2:   ', '-ssl2')
        quick_ssl_test(ssh, ip, 'SSLv3:   ', '-ssl3')
        quick_ssl_test(ssh, ip, 'TLSv1:   ', '-tls1')
        quick_ssl_test(ssh, ip, 'TLSv1.1: ', '-tls1_1')
        quick_ssl_test(ssh, ip, 'TLSv1.2: ', '-tls1_2')


def quick_scan(urls):
    for r in urls:
        ip = r[0]
        desc = r[1]

        try:
            ssh = connect_scan_server(ip, desc)
            if ssh is not None:
                quick_scan_ip(ip, desc)
                # ssh.close()
        except:
            # print server_name, ",", server_desc, ", Errorrrrrrrrrrrrrrrrrrrrrrr"
            raise


def get_ip(dns_name):
    try:
        ip = socket.gethostbyname(dns_name)
        return ip
    except:
        return None


# Scan all possible VIPAs of the web servers (to be on a safe side)
def quick_scan_web_server(server, desc):
    ip = get_ip(server)
    if ip is None:
        print "Cannot get an IP for a server:", server, desc

    quick_scan_ip(ip, server + "(" + ip + ") - " + desc)
    for s in [(server + "-vipa0" + str(l + 1)) for l in range(9)]:
        ip = get_ip(s)
        if ip is not None:
            quick_scan_ip(ip, s + "(" + ip + ") - " + desc)

            # ssh.close()


def quick_scan_server(ip, p, desc):
    global port
    port = p
    ssh = connect_scan_server(ip, desc)
    if ssh is not None:
        quick_scan_ip(ip, desc)


def quick_scan_web_servers(srvrs):
    for s in srvrs:
        quick_scan_web_server(s[0], s[1])


def scan_ssh_ciphers(server, desc):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(server, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")
    t = ssh.get_transport()
    so = t.get_security_options()
    print so.kex
    print so.ciphers
    ssh.close()


# scan_ssh_ciphers('r4pvwb1045', '')

###############################################################################
# Nessus scan issues
###############################################################################

# WSV
# quick_scan_server(get_ip('n4pvwb011'), '443', 'n4pvwb011 - Nessus finding SSL Version 2 and 3 Protocol Detection, SSL Medium Strength Cipher Suites Supported')
# quick_scan_server(get_ip('n4pvwb012'), '443', 'n4pvwb012 - Nessus finding SSL Version 2 and 3 Protocol Detection, SSL Medium Strength Cipher Suites Supported')
# quick_scan_server(get_ip('n4pvwb009'), '22', 'n4pvwb009 - Nessus finding SSH Server CBC Mode Ciphers Enabled, SSH Weak MAC Algorithms Enabled')
# quick_scan_server(get_ip('n4pvwb010'), '22', 'n4pvwb010 - Nessus finding SSH Server CBC Mode Ciphers Enabled, SSH Weak MAC Algorithms Enabled')
# quick_scan_server(get_ip('r4pvap1007'), '22', 'r4pvap1007 - Nessus finding SSH Server CBC Mode Ciphers Enabled, SSH Weak MAC Algorithms Enabled')

# quick_scan_server(get_ip('r5cvwb1003.1dc.com'), '80', 'r5cvwb1003.1dc.com - HTTP TRACE / TRACK Methods Allowed')

# server = 'n4pvwb012-vipa04'
# quick_scan_server(get_ip(server), '443', server)

# MOP
# quick_scan_server(get_ip('a4pvap001'), '11012', 'a4pvap001 - 11012')
# quick_scan_server('10.68.37.112', '443', 'mop.firstdata.eu 443')
# quick_scan_server('10.72.68.46', '443', 'n5pvwb1010-vipa03 accountmanager.firstdatams.com')
# quick_scan_server('10.72.68.48', '443', 'n5pvwb1010-vipa05 merchantportal.firstdata.at')




# AppGuard


###############################################################################
# Preparations
###############################################################################

# distribute_script()


###############################################################################
# External URLs (links to apps, on load balancer in most cases)
###############################################################################

# full_scan(mop_urls)
# full_scan(wsv_urls1)

# quick_scan(appguard_urls) # clean
# quick_scan(wsv_urls)
# quick_scan(wsv_urls1)
# quick_scan(misc_urls) # clean
# quick_scan(mop_urls) # new servers clean, only AIX supports TLS1
# quick_scan(tokenizer_urls) # new servers clean, old need to be switched off

# quick_scan(ewas_urls)
# quick_scan(telecash_urls)  # clean
# quick_scan(cognos_urls)

###############################################################################
# Web servers scans
###############################################################################

# quick_scan_web_servers(Servers.get_servers("MOP-PROD-DR", "Web server"))
# quick_scan_web_servers(Servers.get_servers("WSV-PROD-DR", "Web server"))

# quick_scan_web_servers(Servers.get_servers("MOP", "Web server"))
# quick_scan_web_servers(Servers.get_servers("WSV", "Web server"))
# quick_scan_web_servers(Servers.get_servers("MISC", "Web server"))


###############################################################################
# Application servers stunnel scans
###############################################################################

# quick_scan_server(get_ip('n5bvap994'), '10009', 'WSV PROD DR app server stunnel')
# quick_scan_server(get_ip('r5cvap1003'), '9209', 'WSV UAT app server ESP stunnel')
# quick_scan_server(get_ip('r5cvap1003'), '9009', 'WSV UAT app server EDP stunnel')
# quick_scan_server(get_ip('r5cvap1003'), '10009', 'WSV UAT app server WebLogin stunnel')
# quick_scan_server(get_ip('n4cvap1005'), '10009', 'MISC UAT app server WebLogin stunnel')
# quick_scan_server(get_ip('n4cvap1006'), '10009', 'MISC UAT app server WebLogin stunnel')

# port = '8443'
# port = '58989'
# quick_scan_web_server('r5pvap1001', 'test')
# quick_scan_web_server('n5dvap996', 'test')



# quick_scan_ip("10.74.68.128","")
# quick_scan_ip("10.74.68.129","")
# quick_scan_ip("10.72.68.126","")
# quick_scan_ip("10.72.68.127","")
# quick_scan_ip("10.77.17.238","")
# quick_scan_ip("10.74.68.130","")
# quick_scan_ip("10.74.68.131","")
# quick_scan_ip("10.74.74.52","")
# quick_scan_ip("10.74.74.54","")
# quick_scan_ip("10.72.74.52","")
# quick_scan_ip("10.72.74.54","")
# quick_scan_ip("10.74.74.48","")
# quick_scan_ip("10.74.74.50","")
# quick_scan_ip("10.72.74.48","")
# quick_scan_ip("10.72.74.50","")
# quick_scan_ip("10.77.17.200","")
# quick_scan_ip("10.74.74.90","")
# quick_scan_ip("10.74.74.96","")
# quick_scan_ip("10.72.74.84","")
# quick_scan_ip("10.72.74.90","")
# quick_scan_ip("10.74.74.91","")
# quick_scan_ip("10.74.74.97","")
# quick_scan_ip("10.72.74.85","")
# quick_scan_ip("10.72.74.91","")
quick_scan_ip("10.77.17.230","")


print json.dumps(ips, indent=2, sort_keys=True)
