import os
import tarfile

import paramiko

import Secrets

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())


def excludes_fn(name):
    return ".git" in name or ".idea" in name

tar_folder = "c:\\Users\\tsku1290\\Documents\\Docker"

tar = tarfile.open('archive_name.tgz', 'w:gz')
print os.path.basename(tar_folder)
tar.add(tar_folder, arcname="\\", exclude=excludes_fn)
tar.close()

servers = [
    # 'r5cvap1033',
    # 'r4cvap1051',
    # 'r4cvwb1026',
    # 'r5cvwb1019'

    # 'r4pvap1121',
    # 'r5pvap1098',
    'r4pvwb1047',
    'r4pvwb1048',
    'r5pvwb1039',
    'r5pvwb1040',
    'r4pvwb1045',
    'r4pvwb1046',
    'r5pvwb1037',
    'r5pvwb1038'
]

for server in servers:
    print server

    ssh.connect(server, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("rm -rf ~/docker")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("mkdir ~/docker")

    sftp = ssh.open_sftp()
    sftp.put("archive_name.tgz", "archive_name.tgz")
    sftp.close()

    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("tar xf archive_name.tgz -C ~/docker")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("rm archive_name.tgz ")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("chmod a+r ~/docker/*")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("chmod a+x ~/docker/*.sh")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("mv ~/docker/docker-compose-Linux-x86_64 /tmp")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("chown root:root /tmp/docker-compose-Linux-x86_64")

    ssh.close()

os.remove("archive_name.tgz")

