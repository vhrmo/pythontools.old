import paramiko

import Secrets
import Servers

for s in Servers.servers:
    server_name = s[0]
    server_desc = s[1]

    # if "WSV" in server_desc and "PROD" in server_desc:
    # if "FD Batch" in server_desc :
    if "MOP-PROD-Primary-App" in server_desc :
    # if any(s in server_desc for s in ("PROD", "UAT")):
    # if any(s in server_desc for s in ("MOP", "WSV", "MISC", "AppGuard")) and any(s in server_desc for s in ("UAT", "PROD")):
    # if server_name in ("n4pvap016", "n4pvap017"):
    # if 1==1:
    # if all(s in server_desc for s in ("WSV", "APP"))and any(s in server_desc for s in ("UATa", "PROD")):
    # if all(s in server_desc for s in ("WSV", "PROD", "App", )):

        print server_name, ",", server_desc

        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(server_name, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

            # check for the locked account
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uname")
            if "expired" in ssh_stderr.read():
                print server_name, ",", server_desc, ", Password change required!!!!!!!!!!!!!!!!!!!"
            else:
                log_dir = '/fdc/logs'
                # if "MOP" in server_desc:
                #     log_dir = '/logs'
                # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("tar --ignore-failed-read -zcf ~/`hostname`-logs.tar.gz /fdc/logs")
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("find " + log_dir + " -type f -ctime -3 | xargs tar --ignore-failed-read -zcf ~/`hostname`-logs.tar.gz")
                print ssh_stderr.read()
                print ssh_stdout.read()
                sftp = ssh.open_sftp()
                sftp.get(server_name + "-logs.tar.gz", "c:/a__logs/"+server_name+" - " + server_desc + " - logs.tar.gz")
                sftp.close()
                ssh.exec_command("rm ~/*-logs.tar.gz")

            ssh.close()
        except Exception as e:
            print e

