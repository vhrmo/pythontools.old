import socket


def telnet_test(server, port, comment = ""):

    try:
        ip = socket.gethostbyname(server)

    except:
        ip = "Error"

    try:
        sock = socket.socket()
        sock.connect((server, port))
        connTest = 'ok'

    except:
        connTest = 'Error'

    print server, ';', ip, ';', port, ';', connTest, ';', comment


telnet_test("d4pvir1000", 4505)
telnet_test("d4pvir1000", 4506)
telnet_test("nexus.1dc.com", 8081)



