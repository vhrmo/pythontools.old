import os
import time

import RemoteExec.servers

f = open('result.out', 'a')


def write(txt):
    print(txt)
    f.write(txt)
    f.write("\n")
    f.flush()


write("==================================")
write("Start: " + time.ctime())

for s in RemoteExec.servers.servers:

    # res = 1
    res = os.system('ssh -o BatchMode=yes tsku1290@' + s[0] + ' echo ok &> /dev/null')
    # res = os.system('ssh tsku1290@' + s[0] + ' echo ok &> /dev/null')

    t = "{0:<10}".format(s[0]) + "; " + "{0:<40}".format(s[1]) + "; " + str(res)
    write(t)


f.close()
