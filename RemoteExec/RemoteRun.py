import re

import Secrets
import Users
import paramiko
import Servers


def check_access(ssh, server_name, server_desc):
    print server_name, ",,", "ok"


# Basic server checks
def check_stunnel(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ps -ef | grep stun")
    # out = ssh_stdout.read()
    count = 0
    for line in ssh_stdout:
        if "stunnel" in line:
            count = count + 1

    if count == 0:
        print server_name + "," + server_desc + ", Stunnel not running !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    elif count == 1:
        print server_name + "," + server_desc + ", Stunnel check status !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    else:
        print server_name + "," + server_desc + ", Stunnel OK"


def check_dirty_cow(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("rpm -q kernel-default kernel-`uname -r` --changelog | grep 2016-5195")
    out = ssh_stdout.read()
    if "2016-5195" in out:
        print server_name + ",," + "ok"
    else:
        print server_name + "," + server_desc + ", Dirty Cow patch CVE-2016-5195 missing !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"


def check_open_ssl(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("openssl version")
    out = ssh_stdout.read()
    print server_name + "," + server_desc + "," + out


def download_esp_gc_logs(ssh, server_name, server_desc):
    sftp = ssh.open_sftp()
    sftp.get('/fdc/logs/jboss/esp/gc-esp.log', "c:/a__logs/gc-esp-"+server_name+" - " + server_desc)
    sftp.close()
    # print ssh_stdout.read()
    print server_name + " - " + server_desc + " - done"


def download_mop_gc_logs(ssh, server_name, server_desc):
    tar_file_name = 'gc-logs-' + server_name + '.tar'
    tar_path = '~/' + tar_file_name
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command('find /logs -name "gc*" | xargs tar cf ' + tar_path)

    sftp = ssh.open_sftp()
    sftp.get(tar_file_name, 'c:/a__logs/'+tar_file_name)
    sftp.close()
    # print ssh_stdout.read()
    print server_name + " - " + server_desc + " - done"


def find_card_nums_in_logs(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("grep --max-count=5 --regexp='\([345]\{1\}[0-9]\{3\}\|6011\)\{1\}[ -]\?[0-9]\{4\}[ -]\?[0-9]\{2\}[-]\?[0-9]\{2\}[ -]\?[0-9]\{4\}[^0-9]\{1\}' -r /fdc/logs > tmp_pans")
    sftp = ssh.open_sftp()
    sftp.get('tmp_pans', "c:/a/pans_"+server_name+" - " + server_desc)
    sftp.close()
    # print ssh_stdout.read()
    print server_name + " - " + server_desc + " - done"


def check_not_readable_logs(ssh, server_name, server_desc, log_dir):
    # find all file not readable to others - needed for Splunk to be readable for others
    print "==================================================================================================="
    print "Server>", server_name, server_desc
    print "==================================================================================================="
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("find "+log_dir+" ! -perm -o+r")
    print ssh_stdout.read()


def check_splunk(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ps -ef | grep splunk | grep process-runner")
    out = ssh_stdout.read()
    if "splunkd" in out:
        # print server_name, server_desc, " - Splunk active"
        if "root" in out:
            print server_name + "," + server_desc + ", Splunk active as root"
        else:
            print server_name + "," + server_desc + ", Splunk active as splunk"
    else:
        print server_name + "," + server_desc + ", Splunk not active !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"


def check_appd_dynamics(ssh, server_name, server_desc):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ps -ef | grep -i appd")
    out = ssh_stdout.read()
    if "MachineAgent" in out:
        # print server_name, server_desc, " - Splunk active"
        print server_name + "," + server_desc + ", AppDynamics running"
    else:
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ls -la /opt/appd")
        out = ssh_stdout.read()
        if "current" in out:
            print server_name + "," + server_desc + ", AppDynamics installed"
        else:
            print server_name + "," + server_desc + ", AppDynamics not installed !!!!!!!!!!!!!!!!!!!!!!!!!!!!!"


def print_services(ssh, server_name, server_desc):
    print "==================================================================================================="
    print "Server>", server_name, server_desc
    print "==================================================================================================="
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ps -ef | grep java")
    print ssh_stdout.read()


def check_users(ssh, server_name, server_desc, user_list):
    # print "Server>", server_name, server_desc, "==========================================="
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("getent passwd")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("id ")
    out = ssh_stdout.read()
    # print out
    for u in user_list:
        if Users.get_user_id(u) not in out:
            print "User", u, "[", Users.get_user_id(u), "] not available on ", server_name, server_desc

    # sftp_client = ssh.open_sftp()
    # remote_file = sftp_client.open('/etc/passwd')
    # try:
    #     for line in remote_file:
    #         # process line
    # finally:
    #     remote_file.close()


def run_script_remotely(ssh, server_name, server_desc, script_file_name):
    print "==================================================================================================="
    print "Server>", server_name, server_desc

    ssh.exec_command("rm ~/" + script_file_name)
    sftp = ssh.open_sftp()
    sftp.put(script_file_name, script_file_name)
    sftp.close()
    ssh.exec_command("chmod a+x ~/" + script_file_name)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("python " + script_file_name)
    print ssh_stdout.read()
    ssh.exec_command("rm ~/" + script_file_name)


def snapshot_services(ssh, server_name, server_desc):
    print "Server>", server_name, server_desc
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ps -ef")
    # print ssh_stdout.read()

    f = open('c:/a/ps-ef '+server_name+' - '+server_desc, 'w')
    f.write(ssh_stdout.read())
    f.close()


def check_log_struct(ssh, server_name, server_desc, log_dir):
    print "==================================================================================================="
    print "Server>", server_name, server_desc
    print "==================================================================================================="
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cd /fdc/logs; tree -h -I *.gz -D | grep -v html")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ls -laR /fdc/logs| grep -v html")
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("find /fdc/logs -type f -ctime -3")
    print ssh_stderr.read()
    print "-----------"
    print ssh_stdout.read()
    print "---------------------------------------------------------------------------------------------------"


def print_log_files(ssh, server_name, server_desc, log_dir):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("find /fdc/logs -type f -ctime -3")
    # print ssh_stderr.read()

    with open("LogFiles.csv", "a") as aFile:

        for l in ssh_stdout.read().split("\n"):
            if len(l) > 0:
                print server_name + ';' + l
                aFile.write(server_name + ';' + l + "\n")


def check_os_version(ssh, server_name, server_desc):
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cd /fdc/logs; tree -h -I *.gz -D | grep -v html")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uname -a;lsb_release -a;cat /etc/issue.net; cat /etc/*release")
    out = ssh_stdout.read()
    err = ssh_stderr.read()
    if "Oracle Linux Server release 6.10" in out:
        print "Server>,", server_name, ",", server_desc, ", Oracle Linux 6.10"
    elif "Red Hat Enterprise Linux Server release 6.8" in out:
        print "Server>,", server_name, ",", server_desc, ", RedHat 6.8"
    elif "Red Hat Enterprise Linux Server release 6.10" in out:
        print "Server>,", server_name, ",", server_desc, ", RedHat 6.10"
    elif "Oracle Linux Server release 7.6" in out:
        print "Server>,", server_name, ",", server_desc, ", Oracle Linux 7.6"
    elif "Red Hat Enterprise Linux Server release 6.7" in out:
        print "Server>,", server_name, ",", server_desc, ", RedHat 6.7"
    elif "Red Hat Enterprise Linux Server release 6.3" in out:
        print "Server>,", server_name, ",", server_desc, ", RedHat 6.3"
    elif "SUSE Linux Enterprise Server 10" in out:
        print "Server>,", server_name, ",", server_desc, ", SLES 10"
    elif "SUSE Linux Enterprise Server 11" in out:
        print "Server>,", server_name, ",", server_desc, ", SLES 11"
    else:
        print "==================================================================================================="
        print "Server>", server_name, server_desc
        print err
        print "-----------"
        print out
        print "---------------------------------------------------------------------------------------------------"


docker_versio_rex = re.compile('Version:\s*(\S+)', re.MULTILINE)

def check_docker(ssh, server_name, server_desc):
    # ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cd /fdc/logs; tree -h -I *.gz -D | grep -v html")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("docker version;docker images")

    #  docker images
    # Cannot connect to the Docker daemon. Is the docker daemon running on this host?

    out = ssh_stdout.read()
    err = ssh_stderr.read()

    if "command not found" in err:
        print "Server>,", "{0:<10}".format(server_name), ",", "{0:<35}".format(server_desc), "{0:<28}".format(","), ", Err"

    elif "Version:" in out:
        match = docker_versio_rex.search(out)
        docker_version = match.group(1)

        if "REPOSITORY" in out :
            run_status = " and running"
        else:
            run_status = ", but NOT running"

        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("docker-compose")
        out = ssh_stdout.read()
        err = ssh_stderr.read()
        # print err
        if "Define and run multi-container applications with Docker." not in err:
            run_status += " (docker-compose missing)"

        print "Server>,", "{0:<10}".format(server_name), ",", "{0:<35}".format(server_desc), ", Docker version:", "{0:<10}".format(docker_version), ", OK - Docker installed" + run_status
    else:
        print "==================================================================================================="
        print "Server>", server_name, server_desc
        print err
        print "-----------"
        print out
        print "---------------------------------------------------------------------------------------------------"

def print_hosts_file(ssh, server_name, server_desc):
    get_hosts_file(ssh, server_name, server_desc)


def get_hosts_file(ssh, server_name, server_desc, _print_=False):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cat /etc/hosts")
    err = ssh_stderr.read()
    comment = ""
    prev_line_comment = False
    result = {
        "server_name": server_name,
        "server_desc": server_desc,
        "ips": [],
        "names": []
    }
    for line in ssh_stdout:
        line = line.strip()
        if line == "#":  # skip empty comment line
            comment = ""
            continue
        if line.startswith("#"):
            c = line[1:].strip()
            if prev_line_comment:
                comment = comment + " " + c
            else:
                comment = c
            if comment.startswith("IP-Address"):
                comment = ""
            prev_line_comment = True

        else:
            prev_line_comment = False
            try:
                if line == "":  # skip empty lines
                    continue

                l = line.split('#')  # split comment and data
                if len(l) > 1:  # line contains also comment
                    comment = comment + " " + l[1]

                data = l[0].split()  # split hosts records
                if (data[0] == u"127.0.0.1") or (data[0] == u"127.0.0.2"):
                    continue

                if "-bk" in data[1]:
                    comment = ""
                    continue  # skip backup net

                if data[1].lower() in ("n5pvap010", "n4pvap022", "defra1srvntp1.1dc.com","defra2srvntp1.1dc.com", "deenv01-lc2", "deenv01-lc2.1dc.com", "w4ppir003", "w4ppir005-bk", "w4ppir009-bk", "w5ppir003", "w5ppir005-bk", "w5ppir008", "w5ppir008-bk", "w5ppir009", "w5ppir009-bk"):
                    comment = ""
                    continue  # skip backup and log servers

                if _print_:
                    res = "VIPs," + server_desc + "," + server_name + "," + comment + ","
                    for v in data:
                        res = res + v.lower() + ","
                    print res

                # result["ips"][data[0]]=

            except Exception as e:
                print line
                print e
        # print line


# Blocking call to ssh.exec_command
def exec_command(ssh, cmd):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
    exit_status = ssh_stdout.channel.recv_exit_status()
    if exit_status != 0:
        print("Error running command:", cmd)


def get_user_list(ssh, server_name, server_desc, log_dir):
    exec_command(ssh, "rm -rf ~/logs")
    exec_command(ssh, "mkdir ~/logs")
    exec_command(ssh, "cp -r " + log_dir + "/weblogin*/* ~/logs/")
    exec_command(ssh, "cd ~/logs; for file in *.gz; do gunzip $file; done")
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("grep -h 'everything is ok for user:' ~/logs/*")
    out = ssh_stdout.read()
    err = ssh_stderr.read()

    regex = re.compile('everything is ok for user: (.+)', re.MULTILINE)
    matches = regex.findall(out)
    user_list = set(matches)
    # print len(user_list)
    print server_name, server_desc, len(user_list), "users"

    exec_command(ssh, "rm -rf ~/logs")

    # print user_list
    return user_list

def print_login_stats(ssh, server_name, server_desc, log_dir):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("cat /etc/hosts")
    err = ssh_stderr.read()
    comment = ""
    prev_line_comment = False


def check_disk_usage(ssh, server_name, server_desc, log_dir):
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("df -h | grep logs")
    print "Server: ", server_name, server_desc
    print ssh_stdout.read()


user_list = set()

for s in Servers.servers:
    server_name = s[0]
    server_desc = s[1]


    # if "FD Batch" in server_desc and "PROD" in server_desc:
    # if "Web" in server_desc:
    # if "Tokenizer" not in server_desc :
    # if "AppGuard" in server_desc:
    # if "MOP" in server_desc:
    # if any(s in server_desc for s in ("UAT", "PROD")):
    # if any(s in server_desc for s in ("MOP", "WSV", "MISC", "AppGuard", "Telecash")) and any(s in server_desc for s in ("PROD", "UAT", "SIT")):
    # if any(s in server_desc for s in ("eMOP", "WSV", "eMISC", "eAppGuard")) and any(s in server_desc for s in ("UAT", "PROD")):
    # if "WSV" in server_desc and any(s in server_desc for s in ("UAT", "PROD")) and "Unused" not in server_desc:
    # if server_name == "r5cvwb1003":
    # if server_name in ("a4pvap001", "n4pvap016", "n4pvap017"):
    # if server_name in ("n4pvwb005", "n4pvwb006", "n5pvwb002", "n5pvwb003"):
    # if server_name in ("n4cvwb998", "n4cvwb999", "n5cvwb999", "r4cvwb1012", "r4cvwb1013", "r5cvwb1011", "L5QVWB1006"):
    # if all(s in server_desc for s in ("WSV", "PROD", "JBoss App")):
    # if all(s in server_desc for s in ("MOP", "UAT", "App")):
    # if all(s in server_desc for s in ("MOP", "PROD")):
    if all(s in server_desc for s in ("MOP", "UAT")):
    # if 1==1:
    # if "MISC" in server_desc and any(s in server_desc for s in ("UAT", "PROD")):
    # if "MOP" in server_desc and any(s in server_desc for s in ("UAT", "PROD")):

        try:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(server_name, username=Secrets.user_name, key_filename="c:\work\Keys\private.key")

            log_dir = '/fdc/logs'
            # if "MOP" in server_desc:
            #     log_dir = '/logs'
            if "App" in server_desc:
                log_dir = '/fdc/logs/jboss'
            elif "Web" in server_desc:
                log_dir = '/fdc/logs/apache'

            # check for the locked account
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("uname")
            if "expired" in ssh_stderr.read():
                print server_name, ",", server_desc, ", Password change required!!!!!!!!!!!!!!!!!!!"
            else:

                # Basic server checks
                # check_access(ssh, server_name, server_desc)
                # check_os_version(ssh, server_name, server_desc)
                check_docker(ssh, server_name, server_desc)
                # check_stunnel(ssh, server_name, server_desc)
                # check_dirty_cow(ssh, server_name, server_desc)
                # check_splunk(ssh, server_name, server_desc)
                # check_appd_dynamics(ssh, server_name, server_desc)
                # check_open_ssl(ssh, server_name, server_desc)
                # check_disk_usage(ssh, server_name, server_desc, log_dir)

                # Logs
                # check_not_readable_logs(ssh, server_name, server_desc, log_dir)
                # check_log_struct(ssh, server_name, server_desc, log_dir)
                # print_log_files(ssh, server_name, server_desc, log_dir)

                # print_login_stats(ssh, server_name, server_desc, log_dir)
                # user_list.update(get_user_list(ssh, server_name, server_desc, log_dir))

                # print_services(ssh, server_name, server_desc)
                # snapshot_services(ssh, server_name, server_desc)
                # download_gc_logs(ssh, server_name, server_desc)
                # download_mop_gc_logs(ssh, server_name, server_desc)


                # check_users(ssh, server_name, server_desc, {"Hrmo", "Pelle", "Puskar"})
                # print_hosts_file(ssh, server_name, server_desc)

                # find_card_nums_in_logs(ssh, server_name, server_desc)


                # run_script_remotely(ssh, server_name, server_desc, "EdekaConnectivityTest.py")
                # run_script_remotely(ssh, server_name, server_desc, "SaltConnectivityTest.py")
                # run_script_remotely(ssh, server_name, server_desc, "EdoksConnectivityTest.py")

            ssh.close()
        except:
            print server_name, ",", server_desc, ", Errorrrrrrrrrrrrrrrrrrrrrrr"
            # raise

print "Total nr. of unique users:", len(user_list)

