import socket


connections = [
    ['10.77.17.93', '10.77.17.127', 1433, 'n5dvap998->W5DVDB981:1433 - Access to NSP Test DB'],
    # ['10.57.9.3', '10.77.17.127', 1433, 'Radovan Polakovic->W5DVDB981:1433 - Access to NSP Test DB'],
    # ['10.57.9.53', '10.77.17.127', 1433, 'Lysy->W5DVDB981:1433 - Access to NSP Test DB'],
    ['10.57.9.82', '10.77.17.127', 1433, 'Hrmo->W5DVDB981:1433 - Access to NSP Test DB'],
    # ['10.57.9.64', '10.77.17.127', 1433, 'Bella->W5DVDB981:1433 - Access to NSP Test DB'],


    ['10.68.5.189', '10.77.17.127', 1433, 'n4cvap998->W5DVDB981:1433 - Access to NSP Test DB'],
    ['10.68.5.189', '10.68.13.135', 1433, 'n4cvap998->W4QVDB996:1433 - Access to QA NSP interface DB'],


    ['10.68.6.117', '10.68.13.153', 1433, 'n4pvap1002->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.68.6.121', '10.68.13.153', 1433, 'n4pvap1003->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.68.6.122', '10.68.13.153', 1433, 'n4pvap1004->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.68.6.196', '10.68.13.153', 1433, 'n4pvap1029->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.68.6.197', '10.68.13.153', 1433, 'n4pvap1030->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.5.244', '10.68.13.153', 1433, 'n5pvap1002->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.5.248', '10.68.13.153', 1433, 'n5pvap1003->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.5.249', '10.68.13.153', 1433, 'n5pvap1004->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.6.41', '10.68.13.153', 1433, 'n5pvap1015->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.6.42', '10.68.13.153', 1433, 'n5pvap1016->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.68.4.7', '10.68.13.153', 1433, 'r4pvap1049->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],
    ['10.77.4.168', '10.68.13.153', 1433, 'r5fvap1002->W4PVDB063:1433 - Access to PROD primary NSP interface DB from FDBatch servers'],

    ['10.68.6.117', '10.77.13.118', 1433, 'n4pvap1002->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.68.6.121', '10.77.13.118', 1433, 'n4pvap1003->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.68.6.122', '10.77.13.118', 1433, 'n4pvap1004->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.68.6.196', '10.77.13.118', 1433, 'n4pvap1029->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.68.6.197', '10.77.13.118', 1433, 'n4pvap1030->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.5.244', '10.77.13.118', 1433, 'n5pvap1002->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.5.248', '10.77.13.118', 1433, 'n5pvap1003->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.5.249', '10.77.13.118', 1433, 'n5pvap1004->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.6.41', '10.77.13.118', 1433, 'n5pvap1015->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.6.42', '10.77.13.118', 1433, 'n5pvap1016->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.68.4.7', '10.77.13.118', 1433, 'r4pvap1049->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
    ['10.77.4.168', '10.77.13.118', 1433, 'r5fvap1002->W5BVDB990:1433 - Access to PROD DR NSP interface DB from FDBatch servers'],
]


def telnet_test(server, port, comment = ""):

    try:
        ip = socket.gethostbyname(server)

    except:
        ip = "Error"

    try:
        sock = socket.socket()
        sock.connect((server, port))
        connTest = 'ok'

    except:
        connTest = 'Error'

    print server, ';', ip, ';', connTest, ';', comment


local_ip = socket.gethostbyname(socket.gethostname())

for con in connections:
    source_ip = con[0]
    dest_ip = con[1]
    port = con[2]
    comment = con[3]

    if local_ip == source_ip:
        telnet_test(dest_ip, port, comment)


